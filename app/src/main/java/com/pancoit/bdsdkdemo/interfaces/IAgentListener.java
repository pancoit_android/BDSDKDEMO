package com.pancoit.bdsdkdemo.interfaces;

/**
 * - @Description:  代理监听器
 * - @Author:  LXJ
 * - @Time:  2019/11/14 13:41
 */
public interface IAgentListener {
    /**
     * 盒子连接成功
     */
    void onConnectSuccess();

    /**
     * 盒子断开连接成功
     */
    void onDisconnectSuccess();

    /**
     * 收到消息
     */
    void onMessageReceived(String target);

    /**
     * 指令反馈
     */
    void onCommandFeedback(String target);

    /**
     * 打印所有收到的指令
     */
    void onAllCommandPrint(String command);

    /**
     * SOS信息返回
     *
     * @param centerNumber 中心号码
     * @param frequency    频度
     * @param content      内容
     */
    void onSOSInfoReceived(String centerNumber, int frequency, String content);

    /**
     * 极限追踪信息返回
     *
     * @param centerNumber 中心号码
     * @param frequency    频度
     */
    void onLimitTrackingInfoReceived(String centerNumber, int frequency);

    /**
     * OK键信息返回
     *
     * @param centerNumber 中心号码
     * @param content      内容
     */
    void onOKInfoReceived(String centerNumber, String content);

    /**
     * 版本信息返回
     *
     * @param version
     */
    void onVersionInfoReceived(String version);
}

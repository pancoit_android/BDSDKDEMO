package com.pancoit.bdsdkdemo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * - @Description:  指令输入页面设置
 * - @Author:  LXJ
 * - @Time:  2019/11/21 16:59
 */
public class CommandSettingActivity extends BaseActivity {
    @BindView(R.id.output_filter_layout)
    RelativeLayout outputFilterLayout;
    @BindView(R.id.output_format_layout)
    RelativeLayout outputFormatLayout;

    @Override
    public int getLayoutId() {
        return R.layout.activity_command_setting;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.output_filter_layout, R.id.output_format_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.output_filter_layout:
                break;
            case R.id.output_format_layout:
                break;
        }
    }
}


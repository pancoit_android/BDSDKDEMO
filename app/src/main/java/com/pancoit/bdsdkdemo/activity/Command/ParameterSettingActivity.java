package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParameterSettingActivity extends BaseActivity {


    @BindView(R.id.editText3)
    EditText addr;
    @BindView(R.id.editText4)
    EditText loc;
    @BindView(R.id.editText5)
    EditText fre;
    @BindView(R.id.editText6)
    EditText count;

    @Override
    public int getLayoutId() {
        return R.layout.activity_parameter_setting;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "参数设置(CCPRS)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void OnClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String a = addr.getText().toString();
                String l = loc.getText().toString();
                String f = fre.getText().toString();
                String c = count.getText().toString();
                String instruct = "CCMDS," + a + "," + l + ","+ f + "," + c + ",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct, instruct);
                setResult(Constant.ResultCode, intent);
                finish();
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}


package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LimitTrackingSettingActivity extends BaseActivity {
    @BindView(R.id.opstion)
    EditText opstion;
    @BindView(R.id.car)
    EditText car;
    @BindView(R.id.fre)
    EditText fre;
    @BindView(R.id.mode)
    EditText mode;

    @Override
    public int getLayoutId() {
        return R.layout.activity_limit_tracking_setting;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true,"极限追踪模式设置/查询(CCZZM)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void OnClick(View view){
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String Fre = fre.getText().toString();
                String instruct = "CCZZM," + opstion.getText().toString() +","+ car.getText().toString() +","+ Fre +","+ mode.getText().toString() +",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct,instruct);
                setResult(Constant.ResultCode,intent);
                finish();
                break;
        }
    }


}

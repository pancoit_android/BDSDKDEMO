package com.pancoit.bdsdkdemo.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdkdemo.entity.UserMessage;
import com.pancoit.bdsdkdemo.entity.dao.SessionDao;
import com.pancoit.bdsdkdemo.entity.dao.UserMessageDao;
import com.pancoit.bdsdkdemo.entity.emums.MessageIOType;
import com.pancoit.bdsdkdemo.entity.emums.MessageStatusType;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;
import com.pancoit.bdsdkdemo.utils.TimeUtil;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.Thread.sleep;

/**
 * - @Description:  发送消息页面
 * - @Author:  LXJ
 * - @Time:  2019/11/15 15:34
 */
public class SendMessageActivity extends BaseActivity {
    //卡号输入框
    @BindView(R.id.card_number_edit)
    EditText cardNumberEdit;
    //内容输入框
    @BindView(R.id.content_edit)
    EditText contentEdit;
    //发送按钮
    @BindView(R.id.send_btn)
    Button sendBtn;

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/
    private boolean isRefreshing = false;

    @Override
    public int getLayoutId() {
        return R.layout.activity_send_message;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "发送消息");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        autoRefresh();
    }

    /**
     * 发送消息
     */
    private void sendMesssage() {
        if (!TerminalParams.isConnectNormal) {
            MainApp.getInstance().showMsg("请连接盒子");
            return;
        }
        String cardId = cardNumberEdit.getText().toString().trim();
        if (TextUtils.isEmpty(cardId)) {
            MainApp.getInstance().showMsg("请输入卡号");
            return;
        }
        if (cardId.length() < 6) {
            MainApp.getInstance().showMsg("请输入正常的卡号");
            return;
        }
        String content = contentEdit.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            MainApp.getInstance().showMsg("请输入内容");
            return;
        }
        UserMessage userMessage = new UserMessage();
        userMessage.setToNumber(cardId);
        userMessage.setFromNumber(TerminalParams.cardNumber);
        userMessage.setContent(content);
        userMessage.setMessageType(MessageIOType.OUT.getValue());
        userMessage.setStatus(MessageStatusType.Unknown.getValue());
        userMessage.setTime(TimeUtil.getNowTime());
        //发送消息
        BeidouSDKManage.getInstance().sendMessage(cardId, content);
        //保存会话
        SessionDao.getInstance().save(cardId, TimeUtil.getNowTime(), content);
        //保存聊天数据
        BeidouSDKHandler.lastMsg = UserMessageDao.getInstance().save(userMessage);
        //发送状态倒计时
        BeidouSDKHandler.getInstance().startSentStatusWaitSecTimer();
        //频度倒计时
        MainApp.getInstance().startNewSentWaitSecTimer();
        contentEdit.setText("");
    }

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @OnClick(R.id.send_btn)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_btn:
                sendMesssage();
                break;
        }
    }
    /*******************************************(界面刷新)******************************************************/

    /**
     * 自动刷新
     */
    private void autoRefresh() {
        if (isRefreshing) {
            return;
        }
        isRefreshing = true;
        new Thread(() -> {
            try {
                while (isRefreshing) {
                    Message msg = new Message();
                    msg.what = 1;
                    mHandler.sendMessage(msg);
                    sleep(1000);
                }
            } catch (Exception e) {
                Log.e("InfoMessage", e.toString());
            }
        }).start();
    }

    /**
     * 刷新UI
     */
    private Handler mHandler = new Handler(msg -> {
        switch (msg.what) {
            case 1:
                refreshUiStatus();
                break;
        }
        return false;
    });

    /**
     * 刷新界面
     */
    private void refreshUiStatus() {
        if (TerminalParams.isConnectNormal) {
            if (TerminalParams.sentWaitSec > 0) {
                sendBtn.setText(String.valueOf(TerminalParams.sentWaitSec));
                sendBtn.setClickable(false);
            } else {
                sendBtn.setText("发送");
                sendBtn.setClickable(true);
            }
        } else {
            sendBtn.setText("未连接");
            sendBtn.setClickable(false);
        }
    }
}


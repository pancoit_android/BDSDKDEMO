package com.pancoit.bdsdkdemo.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.activity.Command.SelectCommandActivity;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;
import com.pancoit.bdsdkdemo.constant.GlobalParams;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;
import com.pancoit.bdsdkdemo.utils.PreferencesUtils;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.Thread.sleep;

public class AutoConfigActivity extends BaseActivity {
    @BindView(R.id.editTextNumberSigned)
    EditText number;
    @BindView(R.id.switch1)
    Switch aSwitch;

    @Override
    public int getLayoutId() {
        return R.layout.activity_auto_config;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "连接配置中心号码");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        boolean isAutoConfig = PreferencesUtils.getBoolean(MainApp.getInstance(),GlobalParams.AUTO_CONNECT,false);
        aSwitch.setChecked(isAutoConfig);
        String nb = PreferencesUtils.getString(GlobalParams.AUTO_CONNECT_NUMBER);
        number.setText(nb);
    }

    @OnClick({R.id.button2})
    public void onClick(View view) {
        super.onClick(view);

        if (view.getId() == R.id.button2){
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(number.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            if (aSwitch.isChecked()){
                if (number.getText().toString().equals("")){
                    Toast.makeText(this,"请输入中心号码",Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            PreferencesUtils.setBoolean(this, GlobalParams.AUTO_CONNECT,aSwitch.isChecked());
            PreferencesUtils.setString(this,GlobalParams.AUTO_CONNECT_NUMBER,number.getText().toString());
            Toast.makeText(this,"保存成功",Toast.LENGTH_SHORT).show();
        }
    }
}
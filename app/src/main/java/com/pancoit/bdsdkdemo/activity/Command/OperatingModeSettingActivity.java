package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OperatingModeSettingActivity extends BaseActivity {


    @BindView(R.id.editText3)
    EditText rdss;
    @BindView(R.id.editText4)
    EditText rnss;

    @Override
    public int getLayoutId() {
        return R.layout.activity_operating_mode_setting;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "工作模式设置(CCMDS)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void OnClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String rd = rdss.getText().toString();
                String rn = rnss.getText().toString();
                String instruct = "CCMDS," + rd + "," + rn + ",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct, instruct);
                setResult(Constant.ResultCode, intent);
                finish();
                break;
        }

    }
}


package com.pancoit.bdsdkdemo.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;
import com.pancoit.bdsdklibrary.constant.BeidouSDKParam;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * - @Description:  版本查询
 * - @Author:  LXJ
 * - @Time:  2019/11/15 15:34
 */
public class VersionQueryActivity extends BaseActivity {


    @BindView(R.id.version_edit)
    EditText versionEdit;

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/


    @Override
    public int getLayoutId() {
        return R.layout.activity_version_query;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "版本查询");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        if (!TerminalParams.isConnectNormal) {
            finish();
        }
        BeidouSDKHandler.getInstance().addAgentListener(this);
        versionEdit.setText(TerminalParams.mcuVersion);
        if(BeidouSDKParam.boxVersion==1){

        }else {
            BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCVRQ());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BeidouSDKHandler.getInstance().removeAgentListener(this);
    }

    @Override
    public void onVersionInfoReceived(String version) {
        super.onVersionInfoReceived(version);
        versionEdit.setText(version);
    }
}


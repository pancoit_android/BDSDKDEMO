package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocationApplyActivity extends BaseActivity {


    @BindView(R.id.editText3)
    EditText addr;
    @BindView(R.id.editText4)
    EditText state;
    @BindView(R.id.editText5)
    EditText heightWay;
    @BindView(R.id.editText6)
    EditText heightInstructions;
    @BindView(R.id.editText9)
    EditText height;
    @BindView(R.id.editText10)
    EditText antennaHeight;
    @BindView(R.id.editText11)
    EditText pressure;
    @BindView(R.id.editText12)
    EditText temperature;
    @BindView(R.id.editText13)
    EditText locFre;

    @Override
    public int getLayoutId() {
        return R.layout.activity_location_apply;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "定位申请(CCDWA)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void OnClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String a = addr.getText().toString();
                String s = state.getText().toString();
                String hw = heightWay.getText().toString();
                String hi = heightInstructions.getText().toString();
                String h = height.getText().toString();
                String ah = antennaHeight.getText().toString();
                String p = pressure.getText().toString();
                String t = temperature.getText().toString();
                String l = locFre.getText().toString();
                String instruct = "CCDWA," + a + "," + s + "," + hw + "," + hi + "," + h + "," + ah + "," + p + "," + t + ","+ l + ",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct, instruct);
                setResult(Constant.ResultCode, intent);
                finish();
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}


package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.adapter.SelectCommandRecylerViewAdapter;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SelectCommandActivity extends BaseActivity implements SelectCommandRecylerViewAdapter.OnItemClickListener{

    public static final String key_title = "title";
    private static final String key_activity = "activity";
    private static final String key_comman = "comman";

    List<Map<String, Object>> datas = new ArrayList<>();

    SelectCommandRecylerViewAdapter selectCommandAdapter;
    @BindView(R.id.select_recycle)
    RecyclerView selectRecycle;


    @Override
    public int getLayoutId() {
        return R.layout.activity_select_command;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "选择指令");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        datas.add(setData("3.1.1 工作模式设置(CCMDS)", OperatingModeSettingActivity.class));
        datas.add(setData("3.1.2 工作模式查询(CCMDQ)", "CCMDQ,"));
        datas.add(setData("3.1.3 参数设置(CCPRS)", ParameterSettingActivity.class));
        datas.add(setData("3.1.4 参数查询(CCPRQ)", "CCPRQ,"));
        datas.add(setData("3.1.5 用户组设置(CCUGS)", UserGroupSettingActivity.class));
        datas.add(setData("3.1.6 用户组查询(CCUGQ)", UserGroupQueryActivity.class));
        datas.add(setData("3.1.7 版本查询(CCVRQ)", "CCVRQ,"));
        datas.add(setData("3.1.8 蓝牙设置(CCBTI)", BluetoothSettingActivity.class));
        datas.add(setData("3.1.9 执行关机（CCZGJ）", PerformedShutdownActivity.class));
        datas.add(setData("3.1.10 SOS参数设置/查询(CCSHM)", SOSSettingActivity.class));
        datas.add(setData("3.1.11 启动/关闭SOS救援(CCQJY)", OpenSOSActivity.class));
        datas.add(setData("3.1.12 蓝牙电源控制(CCBTC)", BluetoothPowerActivity.class));
        datas.add(setData("3.1.13 未读短信息／内部打点查询(CCMSQ)", UnreadMessageActivity.class));
        datas.add(setData("3.1.14 极限追踪模式设置/查询(CCZZM)", LimitTrackingSettingActivity.class));
        datas.add(setData("3.1.15 终端自检输出设置(CCZDC)", TerminalSelfinspectionActivity.class));
        datas.add(setData("3.1.16 查询系统当前工作模式(CCMSC)", "CCMSC,"));
        datas.add(setData("3.1.17 定位申请(CCDWA)", LocationApplyActivity.class));
        datas.add(setData("3.1.18 通信申请(CCTXA)", CommunicationApplyActivity.class));
        datas.add(setData("3.1.19 查询北斗卡信息(CCICA)", "CCICA,0,00,"));
        datas.add(setData("3.1.20 终端设置/登录(CCPWD)", TerminalSettingsActivity.class));
        datas.add(setData("3.1.21 FN键功能设置(CCFNS)", FNSettingActivity.class));
        datas.add(setData("3.1.22 FN键查询(CCFNC)", "CCFNC,"));
        datas.add(setData("3.1.23 OK键设置和查询（CCOKS）", OKSettingActivity.class));
        datas.add(setData("3.1.24 启动/关闭极限追踪(CCQZZ)", OpenLimitTrackingActivity.class));
        datas.add(setData("3.1.25 启动/关闭OK追踪(CCQOK)", OpenOKActivity.class));
        datas.add(setData("3.1.26 落水报警参数设置/查询(CCWAH)", WaterAlarmSettingActivity.class));
        datas.add(setData("3.1.27 开机模式设置(CCQDMS)", BootModeActivity.class));
        datas.add(setData("0.0.00 短报文测试(TEST)", SendMessageTestActivity.class));
        selectCommandAdapter = new SelectCommandRecylerViewAdapter(datas,this);
        selectRecycle.setLayoutManager(new LinearLayoutManager(this));

        selectRecycle.setAdapter(selectCommandAdapter);

        selectRecycle.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                outRect.top = 1;
                outRect.bottom = 1;
            }
        });
    }

    private Map<String, Object> setData(String title, Class<?> cl) {
        Map<String, Object> map = new HashMap<>();
        map.put(key_title, title);
        map.put(key_activity, cl);
        return map;
    }

    private Map<String, Object> setData(String title, String comman) {
        Map<String, Object> map = new HashMap<>();
        map.put(key_title, title);
        map.put(key_comman, comman);
        return map;
    }


    @Override
    public void OnItemClickListener(int postion) {
        Map<String,Object> map = datas.get(postion);
         Object obj = map.get(key_activity);
         if (obj != null){
             startActivityForResult(new Intent(this,(Class)obj),Constant.RequestCode);
             return;
         }else {
             // 字符串指令
             String instruct = (String) map.get(key_comman);
             pop(instruct);
         }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constant.ResultCode) {
//            Intent intent = getIntent();
            String instruct = data.getStringExtra(Constant.instruct);
//            intent.putExtra(Constant.instruct,instruct);
//            setResult(Constant.ResultCode,intent);
//            finish();
            pop(instruct);
        }
    }
    public void pop(String instruct){
        Intent intent = getIntent();
        intent.putExtra(Constant.instruct,instruct);
        setResult(Constant.ResultCode,intent);
        finish();
    }
}

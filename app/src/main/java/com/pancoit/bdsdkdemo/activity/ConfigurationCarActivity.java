package com.pancoit.bdsdkdemo.activity;

import static java.lang.Thread.sleep;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;
import com.pancoit.bdsdkdemo.utils.PreferencesUtils;
import com.pancoit.bdsdkdemo.utils.TimeUtil;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;
import com.pancoit.bdsdklibrary.utils.CheckCodeUtil;
import com.pancoit.bdsdklibrary.utils.FormatConversionUtil;
import com.pancoit.bdsdkdemo.utils.PreferencesUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfigurationCarActivity extends BaseActivity {
    @BindView(R.id.centernumber)
    EditText centernumber;
    @BindView(R.id.cCount)
    EditText cCount;
    @BindView(R.id.cFir)
    EditText cFir;
    @BindView(R.id.bleName)
    EditText bleName;
    @BindView(R.id.box_output_tv)
    TextView boxOutputTv;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.box_output_layout)
    LinearLayout boxOutputLayout;

    private static final String NUMBER_KEY = "PANCOIT.COAM.CENTERNUMBER";
    private static final String COUNT_KEY = "PANCOIT.COAM.COUNT_KEY";
    private static final String FRI_KEY = "PANCOIT.COAM.FRI_KEY";
    @Override
    public int getLayoutId() {
        return R.layout.activity_configuration_car;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "车载配置（2.1协议）");
    }

    @Override
    protected void initEvent() {
        BeidouSDKHandler.getInstance().addAgentListener(this);
    }

    @Override
    protected void initData() {
        if (TerminalParams.isConnectNormal) {
            bleName.setText(TerminalParams.cardNumber);
        }

        String num = PreferencesUtils.getString(NUMBER_KEY);
        if (num != null && !num.equals("")){
            centernumber.setText(num);
        }

        String count = PreferencesUtils.getString(COUNT_KEY);
        if (count != null && !count.equals("")){
            cCount.setText(count);
        }
        String fir = PreferencesUtils.getString(FRI_KEY);
        if (fir != null && !fir.equals("")){
            cFir.setText(fir);
        }
    }

    @OnClick(R.id.button)
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() != R.id.button) return;
        if (!TerminalParams.isConnectNormal) {
            MainApp.getInstance().showMsg("请先连接盒子");
            return;
        }

        new Thread(() -> {
            try {
                String number = centernumber.getText().toString();
                String count = cCount.getText().toString();
                String fir = cFir.getText().toString();
                String blename = bleName.getText().toString();

                String v = command("CCYPS,1,1,1");
                addTextToView("工作协议设置版本为2.1协议:"+FormatConversionUtil.hexToString(v));
                BeidouSDKManage.getInstance().sendHexCommand(v);
                sleep(500);

                String t = command("CCPRS,"+number+",3,"+fir+","+count);
                addTextToView("设置上报参数:"+FormatConversionUtil.hexToString(t));
                BeidouSDKManage.getInstance().sendHexCommand(t);
                PreferencesUtils.saveData(ConfigurationCarActivity.this,NUMBER_KEY,number);
                PreferencesUtils.saveData(ConfigurationCarActivity.this,COUNT_KEY,count);
                PreferencesUtils.saveData(ConfigurationCarActivity.this,FRI_KEY,fir);
                sleep(500);
                String n = command("CCBTI,"+blename);
                addTextToView("设置蓝牙名称:"+FormatConversionUtil.hexToString(n));
                BeidouSDKManage.getInstance().sendHexCommand(n);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    private String command(String command){
        String it = "$" + command + "*"+ CheckCodeUtil.getCheckCode0007(FormatConversionUtil.stringToHex(command));
        return FormatConversionUtil.stringToHex(it)+"0D0A";
    }

    /**
     * 添加字符串到TextView
     *
     * @param str
     */
    private void addTextToView(final String str) {
        getActivity().runOnUiThread(() -> {
            boxOutputTv.append("\n\n" + TimeUtil.getNowTime());
            boxOutputTv.append("\n" + str);
            scroll2Bottom(scrollView, boxOutputLayout);
        });
    }
    /**
     * 滚动到底部
     *
     * @param scroll Textview
     * @param inner  Layout
     */
    public static void scroll2Bottom(final ScrollView scroll, final View inner) {
        Handler handler = new Handler();
        handler.post(() -> {
            // TODO Auto-generated method stub
            if (scroll == null || inner == null) {
                return;
            }
            // 内层高度超过外层
            int offset = inner.getMeasuredHeight()
                    - scroll.getMeasuredHeight();
            if (offset < 0) {
                System.out.println("定位...");
                offset = 0;
            }
            scroll.scrollTo(0, offset);
        });
    }
    @Override
    public void onAllCommandPrint(String command) {
        super.onAllCommandPrint(command);
        String str = FormatConversionUtil.hexToString(command);
        if (str.contains("BDYPX") || str.contains("BDPRX") || str.contains("BDBTX")){
            addTextToView(str);
        }
    }
}
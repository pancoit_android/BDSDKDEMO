package com.pancoit.bdsdkdemo.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;
import com.pancoit.bdsdklibrary.constant.BeidouSDKParam;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * - @Description:  SOS功能
 * - @Author:  LXJ
 * - @Time:  2019/11/15 15:34
 */
public class SOSFunctionActivity extends BaseActivity {
    @BindView(R.id.number_edit)
    EditText numberEdit;
    @BindView(R.id.frequency_edit)
    EditText frequencyEdit;
    @BindView(R.id.content_edit)
    EditText contentEdit;
    @BindView(R.id.confirm_btn)
    Button confirmBtn;

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @Override
    public int getLayoutId() {
        return R.layout.activity_sos_function;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "SOS键设置");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        if (!TerminalParams.isConnectNormal) {
            finish();
        }
        BeidouSDKHandler.getInstance().addAgentListener(this);
        numberEdit.setText(TerminalParams.sosCenterNumber);
        frequencyEdit.setText(String.valueOf(TerminalParams.sosFrequency));
        contentEdit.setText(TerminalParams.sosContent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BeidouSDKHandler.getInstance().removeAgentListener(this);
    }

    @OnClick(R.id.confirm_btn)
    public void onClick() {
        if (!TerminalParams.isConnectNormal) {
            MainApp.getInstance().showMsg("请先连接盒子");
            return;
        }
        String number = numberEdit.getText().toString().trim();
        if (TextUtils.isEmpty(number)) {
            MainApp.getInstance().showMsg("请输入中心号码");
            return;
        }
        String frequency = frequencyEdit.getText().toString().trim();
        if (TextUtils.isEmpty(frequency)) {
            MainApp.getInstance().showMsg("请输入频度");
            return;
        }
        String content = contentEdit.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            MainApp.getInstance().showMsg("请输入内容");
            return;
        }
        if (BeidouSDKParam.boxVersion == 2) {
            BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCSHM("1", number, frequency, content));
        }
    }

    @Override
    public void onSOSInfoReceived(String centerNumber, int frequency, String content) {
        super.onSOSInfoReceived(centerNumber, frequency, content);
        MainApp.getInstance().showMsg("设置成功");
    }
}


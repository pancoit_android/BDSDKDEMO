package com.pancoit.bdsdkdemo.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;
import com.pancoit.bdsdkdemo.utils.TimeUtil;
import com.pancoit.bdsdklibrary.constant.BeidouSDKParam;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;
import com.pancoit.bdsdklibrary.utils.FormatConversionUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.Thread.sleep;

public class ConfigurationActivity extends BaseActivity {
    @BindView(R.id.centernumber)
    EditText centernumber;
    @BindView(R.id.sosFer)
    EditText sosFir;
    @BindView(R.id.sosContent)
    EditText sosContent;
    @BindView(R.id.ltFir)
    EditText ltFir;
    @BindView(R.id.okContent)
    EditText okContent;
    @BindView(R.id.bleName)
    EditText bleName;
    @BindView(R.id.box_output_tv)
    TextView boxOutputTv;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.box_output_layout)
    LinearLayout boxOutputLayout;

    @Override
    public int getLayoutId() {
        return R.layout.activity_configuration;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "一键配置");
    }

    @Override
    protected void initEvent() {
        BeidouSDKHandler.getInstance().addAgentListener(this);
    }

    @Override
    protected void initData() {
        if (TerminalParams.isConnectNormal) {
            bleName.setText(TerminalParams.cardNumber);
        }
    }

    @OnClick(R.id.button)
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() != R.id.button) return;
        if (!TerminalParams.isConnectNormal) {
            MainApp.getInstance().showMsg("请先连接盒子");
            return;
        }

        new Thread(() -> {
            try {
                String number = centernumber.getText().toString();
                String sosfir = sosFir.getText().toString();
                String soscontent = sosContent.getText().toString();
                String ltfir = ltFir.getText().toString();
                String okcontent = okContent.getText().toString();
                String blename = bleName.getText().toString();

                BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCZZM("1", number, ltfir, "1"));
                sleep(500);
                BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCSHM("1",number,sosfir,soscontent));
                sleep(500);
                BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCOKS("1", number, okcontent));
                sleep(500);
                BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCBTI(blename));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * 添加字符串到TextView
     *
     * @param str
     */
    private void addTextToView(final String str) {
        getActivity().runOnUiThread(() -> {
            boxOutputTv.append("\n\n" + TimeUtil.getNowTime());
            boxOutputTv.append("\n" + str);
            scroll2Bottom(scrollView, boxOutputLayout);
        });
    }
    /**
     * 滚动到底部
     *
     * @param scroll Textview
     * @param inner  Layout
     */
    public static void scroll2Bottom(final ScrollView scroll, final View inner) {
        Handler handler = new Handler();
        handler.post(() -> {
            // TODO Auto-generated method stub
            if (scroll == null || inner == null) {
                return;
            }
            // 内层高度超过外层
            int offset = inner.getMeasuredHeight()
                    - scroll.getMeasuredHeight();
            if (offset < 0) {
                System.out.println("定位...");
                offset = 0;
            }
            scroll.scrollTo(0, offset);
        });
    }
    @Override
    public void onAllCommandPrint(String command) {
        super.onAllCommandPrint(command);
        String str = FormatConversionUtil.hexToString(command);
        if (str.contains("BDHMX") || str.contains("BDZZX") || str.contains("BDOKX") ){
            addTextToView(str);
        }
    }
}
package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WaterAlarmSettingActivity extends BaseActivity {


    @BindView(R.id.opstion)
    EditText opstion;
    @BindView(R.id.car)
    EditText car;
    @BindView(R.id.prevent_contact)
    EditText preventContact;
    @BindView(R.id.fre1)
    EditText fre1;
    @BindView(R.id.fre2)
    EditText fre2;
    @BindView(R.id.content)
    EditText content;

    @Override
    public int getLayoutId() {
        return R.layout.activity_water_alarm_setting;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true,"落水报警参数设置/查询(CCWAH)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String preventContactFre = preventContact.getText().toString();
                String Fre1 = fre1.getText().toString();
                String Fre2 = fre2.getText().toString();
                String contentText = content.getText().toString();
                if (opstion.getText().toString() != "0"){
                    if (preventContactFre == "") {
                        preventContactFre = "10";
                    }
                    if (Fre1 == "") {
                        Fre1 = "65";
                    }

                    if (Fre2 == "") {
                        Fre2 = "600";
                    }
                    if (contentText == "") {
                        contentText = "MOB ACTIVE";
                    }
                }

                String instruct = "CCWAH," + opstion.getText().toString() +","+ car.getText().toString() +","+ preventContactFre +","+ Fre1 +","+ Fre2 +","+ contentText+",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct,instruct);
                setResult(Constant.ResultCode,intent);
                finish();
                break;
        }
    }
}

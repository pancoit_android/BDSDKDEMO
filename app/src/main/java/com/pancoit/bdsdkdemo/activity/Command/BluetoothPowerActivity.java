package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BluetoothPowerActivity extends BaseActivity {


    @BindView(R.id.editText3)
    EditText type;
    @BindView(R.id.editText4)
    EditText time1;
    @BindView(R.id.editText5)
    EditText time2;

    @Override
    public int getLayoutId() {
        return R.layout.activity_bluetooth_power;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "蓝牙电源控制(CCBTC)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void OnClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String t = type.getText().toString();
                String t1 = time1.getText().toString();
                String t2 = time2.getText().toString();
                String instruct = "CCBTC," + t + "," + t1 + "," + t2 + ",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct, instruct);
                setResult(Constant.ResultCode, intent);
                finish();
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}


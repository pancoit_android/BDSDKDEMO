package com.pancoit.bdsdkdemo.activity.Command;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;
import com.pancoit.bdsdkdemo.utils.TestSendMessageTool;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendMessageTestActivity extends BaseActivity {
    @BindView(R.id.editTextTextPersonName)
    EditText carNumber;
    @BindView(R.id.editTextTextPersonName2)
    EditText frequency;
    @BindView(R.id.editTextTextMultiLine)
    EditText content;

    private View selectMode;
    private View selectProtocol;
    private int[] modes = {R.id.button3,R.id.button4,R.id.button5};
    private int[] protocols = {R.id.button6,R.id.button7,R.id.button8};

    @Override
    public int getLayoutId() {
        return R.layout.activity_send_message_test;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "短报文测试(TEST)");
        selectMode = findViewById(modes[TestSendMessageTool.getInstance().mode-1]);
        selectProtocol = findViewById(protocols[TestSendMessageTool.getInstance().protocol-1]);
        onMode(selectMode);
        onProtocol(selectProtocol);
        frequency.setText(TestSendMessageTool.getInstance().frequency+"");
        carNumber.setText(TestSendMessageTool.getInstance().number);
        continuousSendButton(findViewById(R.id.button10));
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }


    public void onMode(View view){
        TestSendMessageTool.getInstance().mode = Integer.valueOf((String)view.getTag());
        selectMode.setBackgroundResource(R.color.colorTextGray);
        selectMode = view;
        selectMode.setBackgroundResource(R.color.colorPrimary);
    }

    public void onProtocol(View view){
        TestSendMessageTool.getInstance().protocol = Integer.valueOf((String)view.getTag());
        selectProtocol.setBackgroundResource(R.color.colorTextGray);
        selectProtocol = view;
        selectProtocol.setBackgroundResource(R.color.colorPrimary);
    }

    @OnClick({R.id.button9,R.id.button10})
    public void OnClick(View view) {
        super.onClick(view);

        String text = content.getText().toString();
        if (text.isEmpty()) text = "A9000690259E62908CDD141322E4B9BD7956BCFA88F6E87B4D18AAB91C22E02B8A065110C8EE9FA919530527A6AA3665AA638196AF088B8C9388CEFB8E9C7D1E8B750EA8497EC0A74925ED06CB034787C339DC60A11AE4623EF539673AA300F9962A300BCFFBFA34EF599916449FE17B66B33E83213F1077C2FF1522F6E96DE1664EC43CF113EEEAF59492748F37A23E7BA4149DB1462F413E7A762BCA2BC5083BAFFDFDF3F6F501110E009AE468729C529002401DDE2EAD0838B2C5CCE21B6AD61ADBE82BD59A3CBAC4FF58BD06B341C5B5B7516BA928EF7FEFC8C5E8193AC7F9B731D62B2A11D28E8923E5578309B6A46878BD3343D76E286AF2E41A1FE1A81591E2DA4D4BD9086F7BA8E2AB0BE40E8DD69DB62C8F2A4E6363226EDAC24420A903357F89F0A892EC2C9279F62A4A5E84B9F6ECF52430BE814C163B68F13205042CB05792FF56DE55EDB6E37158C584DC96151BF3F134BCE01D7A627060922E345091A5DE642085343426AF2F3726743FC9CB799632713C01F0333711A42DE39DBAC0065A7ADE64C1FBA6FBA8F8FE50FE2075E995DEFE580000B0D9E6AB634F03B6E8096BF4EF4F36989973EDE0BE6635E55B5A823162CC9E9973BD3D80AD004C60362C7762F28EC0C089D0F3EE5A08FD6FD05A577DE3556AD269674840858B770F302DEA51CF842203E12A32D499315440AAEE6916136FD96A1EBB8192D33E2C9E4BCA7EC2D2244CDCF5F17898268BAA06BF99BB1B52392E8415B406DC9294AD389E7839CDDE3055AB06B1471C5AF033E8BDDBB4B8A2E72FA6B62032B325ED35E4ED4B384E673588E30A1D16808F1A1CEC119E6D39235B411D03E4A4ED95BDBDD6219ABF62FA53F86C7B1E159C0595B7AD0CF4BC58E27E2B9B3451359D5EAB65F7E9E3695BCE707FEB7B8F3EAE61426BC2E462A3D2913903E80ACA58AD524070822986879BF1F935977465F426C676001946497F34FF81B73ED1FEFAE4C757442D253B945325805FDC097282DEF7886B9BAC3E08684AC5C7BBC062D9A8126E8D18ED9DCAF48BACCAE2D077FD28B8B583BBD097AA8897B1025ADFD674277E42A7F2883F5C9D6948856C9C6D3823460CC487904A5F60348E42BD9D023FB423E8450DE8DE47214B87AACBDFD0671E5C3B948F50178E7D8B80D3CA01719AE46CE6E131E39DBAD0065A7AE899307EE9DEEA9E3F943F881FBA7FBA17E58000B0D9DDF78A24BA09363559D356949859C8F0AC820EB9FD7EC72E01542288568D97BD9941E8D576B573E171BAE3F015918F887A995D139581F81E5D991756C18D2338246216D5933E887F64043825BDEA51CF8422036CADB9836052EA01A93288DAF04F51A0C6283525340BF0E10D2944670965DE25C341D2F63E30ABDB9B98F6D3726CC239BDA98DFA99196231BB64485C4F078F9D33AC68A0815C8092E8BDDBB4B8A2E72FA6B6118EACFBAB60B2B94AB92E313DB31141E164D3F237E65CEBCA655E545CC3084425EF1D4A939D1FF1EE4CE647FCA248F6247576BB2ECB0433AB0553D9A6472C9A039675274AD3FA8179DFE9E3695BCE707FEB7B8F3EAE61426BC2E462A3D2913903E80ACA58AD52423AF73B311264072ADC040461B8209810B5057B27379C2D300A0C3916EE52A90FBEAC243DEF4A62A5E4E715A52758F8D1A02A19EB40BCE1B5491FA023BEF6A603795ABF07006C254A0BED3EAF5353BD26F3E01F781E8A568B59A58296ECE30BD5103AAAFC42EBD8F674D65D445D051A3DA6E296B90BC1D02572F9237D7FE92463EF9B9A412195BE51582079CE4CF48D2B7298DFD0671E5C3B921E9698120B76419814A8E394FE0E5DCBD5B2D3D6F3260FDD53DD23E1FC7FE38EBD2DF52000F015918F887A995D139581F8A59228ECBD11918EC525E018C1D6597FFDA7EA1FE3E2527F5C826480C06D6BED395D4C7821D6FB3A41A6A9D82C5BA04E016A897F8F7D21C26C1D8879DB0CA2D4838A0E322069E913784B894326F7E668DCEB2FE1EB9E2F729A3C9E63E74EA2172BB47D3B75C9E8BDDBB4B8A2E72FA6B62032B325ED35E4ED4B384D53FF69C7920745A02386DBBE001E23E9B647EF9F9B5EDEB9BB7667814FEEAF2A5700A926EA0659EF2C7ADDF5A072A7064CF0C6E9E3695BCE6FE3EB7BABDBA6436271A004293C8E118EE80ACA58B86B98822985FA21DB9F2E9928F4E892F4DF637EAB8A5849DAEAF9E73BFF7BB7B8BB05DF9033D4735ED5C10C48803D9D775EABE56A3316FAF72F9102906EAD5E0996F19E9722E832B43DE27444FBDFD06721B7F073FF71DBCAA89889A7E43074808080FFD9";

        String number = carNumber.getText().toString();
        if (number.isEmpty())return;
        TestSendMessageTool.getInstance().number = number;

        switch (view.getId()) {
            case R.id.button9:
                //单次发射
                TestSendMessageTool.getInstance().sendMessage(text);
                break;
            case R.id.button10:
                Button button = (Button) view;
                if (TestSendMessageTool.getInstance().isStart){
                    TestSendMessageTool.getInstance().stop();
                }else {
                    //连续发射
                    int fre = Integer.parseInt(frequency.getText().toString());
                    TestSendMessageTool.getInstance().startSendMessage(text,fre);
                }
                continuousSendButton(button);
                break;
        }

    }

    public void continuousSendButton(Button button){
        if (TestSendMessageTool.getInstance().isStart){
            button.setText("取消连续发送");
            button.setBackgroundResource(R.color.colorAccent);
        }else {
            button.setText("按频度连续发送");
            button.setBackgroundResource(R.color.colorPrimary);
        }
    }

    @Override
    public void onAllCommandPrint(String command) {
        super.onAllCommandPrint(command);

    }
}
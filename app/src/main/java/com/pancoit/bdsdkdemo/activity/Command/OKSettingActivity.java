package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OKSettingActivity extends BaseActivity {


    @BindView(R.id.editText3)
    EditText operation;
    @BindView(R.id.editText4)
    EditText card;
    @BindView(R.id.editText8)
    EditText content;

    @Override
    public int getLayoutId() {
        return R.layout.activity_sos_setting;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "OK键设置和查询（CCOKS）");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void OnClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String o = operation.getText().toString();
                String c = card.getText().toString();
                String ct = content.getText().toString();
                String instruct = "CCOKS," + o + ","+ c + "," + ct + ",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct, instruct);
                setResult(Constant.ResultCode, intent);
                finish();
                break;
        }

    }


}


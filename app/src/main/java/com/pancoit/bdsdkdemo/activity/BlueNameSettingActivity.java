package com.pancoit.bdsdkdemo.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdklibrary.constant.BeidouSDKParam;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * - @Description:  蓝牙名称设置
 * - @Author:  LXJ
 * - @Time:  2019/11/15 15:34
 */
public class BlueNameSettingActivity extends BaseActivity {
    @BindView(R.id.blue_name_edit)
    EditText blueNameEdit;
    @BindView(R.id.confirm_btn)
    Button confirmBtn;

    @Override
    public int getLayoutId() {
        return R.layout.activity_bluename_setting;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "蓝牙名称设置");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        blueNameEdit.setText(TerminalParams.blueName);
    }

    @OnClick(R.id.confirm_btn)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_btn:
                //设置蓝牙名称
                if (!TerminalParams.isConnectNormal) {
                    return;
                }
                String blueName = blueNameEdit.getText().toString().trim();
                if (TextUtils.isEmpty(blueName) || blueName.length() > 11) {
                    return;
                }
                if (BeidouSDKParam.boxVersion == 1) {

                } else {
                    BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCBTI(blueName));
                }
                break;
        }
    }
}


package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BootModeActivity extends BaseActivity {


    @BindView(R.id.editText14)
    EditText option;
    @BindView(R.id.editText15)
    EditText mode;

    @Override
    public int getLayoutId() {
        return R.layout.activity_boot_mode;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        initTitleBar(true, "开机模式设置(CCQDMS)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String v1 = option.getText().toString();
                String v2 = mode.getText().toString();
                if (v1 == "") {
                    MainApp.getInstance().showMsg("请输入操作方式");
                    return;
                }

                String instruct = "CCQDMS," + v1 + "," + v2 + ",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct, instruct);
                setResult(Constant.ResultCode, intent);
                finish();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
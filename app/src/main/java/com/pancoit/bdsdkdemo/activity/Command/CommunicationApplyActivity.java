package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommunicationApplyActivity extends BaseActivity {


    @BindView(R.id.editText3)
    EditText addr;
    @BindView(R.id.editText4)
    EditText type;
    @BindView(R.id.editText5)
    EditText form;
    @BindView(R.id.editText6)
    EditText content;

    @Override
    public int getLayoutId() {
        return R.layout.activity_communication_apply;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true, "通信申请(CCTXA)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.confirm)
    public void OnClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String a = addr.getText().toString();
                String t = type.getText().toString();
                String f = form.getText().toString();
                String c = content.getText().toString();
                String instruct = "CCTXA," + a + "," + t + "," + f + "," + c + "," ;
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct, instruct);
                setResult(Constant.ResultCode, intent);
                finish();
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}


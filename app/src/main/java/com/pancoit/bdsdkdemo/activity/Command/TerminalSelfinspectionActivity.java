package com.pancoit.bdsdkdemo.activity.Command;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.base.BaseActivity;
import com.pancoit.bdsdkdemo.constant.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TerminalSelfinspectionActivity extends BaseActivity {


    @BindView(R.id.fre)
    EditText fre;

    @Override
    public int getLayoutId() {
        return R.layout.activity_terminal_selfinspection;
    }

    @Override
    public void initContentView(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        initTitleBar(true,"终端自检输出设置(CCZDC)");
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }
    @OnClick(R.id.confirm)
    public void OnClick(View view){
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm:
                String Fre = fre.getText().toString();
                String instruct = "CCZDC,"+ Fre +",";
                Intent intent = getIntent();
                intent.putExtra(Constant.instruct,instruct);
                setResult(Constant.ResultCode,intent);
                finish();
                break;
        }

    }
}

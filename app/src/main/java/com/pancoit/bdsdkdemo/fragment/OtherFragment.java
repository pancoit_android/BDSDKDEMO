package com.pancoit.bdsdkdemo.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.activity.AutoConfigActivity;
import com.pancoit.bdsdkdemo.activity.BlueNameSettingActivity;
import com.pancoit.bdsdkdemo.activity.ConfigurationActivity;
import com.pancoit.bdsdkdemo.activity.ConfigurationCarActivity;
import com.pancoit.bdsdkdemo.activity.LimitTrackingActivity;
import com.pancoit.bdsdkdemo.activity.OKButtonSettingActivity;
import com.pancoit.bdsdkdemo.activity.SOSFunctionActivity;
import com.pancoit.bdsdkdemo.activity.VersionQueryActivity;
import com.pancoit.bdsdkdemo.base.BaseFragment;
import com.pancoit.bdsdkdemo.constant.TerminalParams;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * - @Description:  状态页
 * - @Author:  LXJ
 * - @Time:  2019/11/12 15:53
 */
public class OtherFragment extends BaseFragment {
    @BindView(R.id.sos_layout)
    RelativeLayout sosLayout;
    @BindView(R.id.limit_tracking_layout)
    RelativeLayout limitTrackingLayout;
    @BindView(R.id.ok_layout)
    RelativeLayout okLayout;
    @BindView(R.id.version_layout)
    RelativeLayout versionLayout;
    @BindView(R.id.bluetooth_name_layout)
    RelativeLayout bluetoothNameLayout;
    @BindView(R.id.about_layout)
    RelativeLayout aboutLayout;

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/
    private Unbinder unbinder;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_other;
    }

    @Override
    public void initContentView(View viewContent) {
        unbinder = ButterKnife.bind(this, viewContent);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.sos_layout, R.id.limit_tracking_layout, R.id.ok_layout, R.id.version_layout, R.id.bluetooth_name_layout, R.id.about_layout,R.id.configuration_layout,R.id.configuration_car_layout,R.id.connect_autoconfig})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sos_layout:
                if (TerminalParams.isConnectNormal) {
                    startActivity(new Intent(getActivity(), SOSFunctionActivity.class));
                } else {
                    MainApp.getInstance().showMsg("请先连接设备");
                }
                break;
            case R.id.limit_tracking_layout:
                if (TerminalParams.isConnectNormal) {
                    startActivity(new Intent(getActivity(), LimitTrackingActivity.class));
                } else {
                    MainApp.getInstance().showMsg("请先连接设备");
                }
                break;
            case R.id.ok_layout:
                if (TerminalParams.isConnectNormal) {
                    startActivity(new Intent(getActivity(), OKButtonSettingActivity.class));
                } else {
                    MainApp.getInstance().showMsg("请先连接设备");
                }
                break;
            case R.id.version_layout:
                if (TerminalParams.isConnectNormal) {
                    startActivity(new Intent(getActivity(), VersionQueryActivity.class));
                } else {
                    MainApp.getInstance().showMsg("请先连接设备");
                }
                break;
            case R.id.bluetooth_name_layout:
                if (TerminalParams.isConnectNormal) {
                    startActivity(new Intent(getActivity(), BlueNameSettingActivity.class));
                } else {
                    MainApp.getInstance().showMsg("请先连接设备");
                }
                break;
            case R.id.about_layout:

                break;
            case R.id.configuration_layout:
                startActivity(new Intent(getActivity(), ConfigurationActivity.class));
                break;
            case R.id.connect_autoconfig:
                startActivity(new Intent(getActivity(), AutoConfigActivity.class));
                break;
                //configuration_car_layout
            case R.id.configuration_car_layout:
                startActivity(new Intent(getActivity(), ConfigurationCarActivity.class));
                break;
        }
    }
}


package com.pancoit.bdsdkdemo.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.activity.MessageItemActivity;
import com.pancoit.bdsdkdemo.activity.SendMessageActivity;
import com.pancoit.bdsdkdemo.adapter.SessionRecylerviwAdapter;
import com.pancoit.bdsdkdemo.base.BaseFragment;
import com.pancoit.bdsdkdemo.entity.Session;
import com.pancoit.bdsdkdemo.entity.dao.SessionDao;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * - @Description:  状态页
 * - @Author:  LXJ
 * - @Time:  2019/11/12 15:53
 */
public class CommunicationFragment extends BaseFragment implements SessionRecylerviwAdapter.OnItemClickListener {
    //发送消息按钮
    @BindView(R.id.send_message_btn)
    Button sendMessageBtn;
    //消息列表
    @BindView(R.id.message_list_view)
    RecyclerView messageListView;

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/
    private Unbinder unbinder;
    //会话列表适配器
    private SessionRecylerviwAdapter adapter;
    //会话列表数据
    private List<Session> mListData;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_communication;
    }

    @Override
    public void initContentView(View viewContent) {
        unbinder = ButterKnife.bind(this, viewContent);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        //初始化列表
        initListView();
        //添加监听器
        BeidouSDKHandler.getInstance().addAgentListener(this);
    }

    /**
     * 初始化列表
     */
    private void initListView() {
        mListData = new ArrayList<>();
        adapter = new SessionRecylerviwAdapter(getActivity(), mListData, this);
        messageListView.setAdapter(adapter);
        messageListView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        getListData();
    }

    /**
     * 获取数据
     */
    private void getListData() {
        mListData.clear();
        List<Session> list = SessionDao.getInstance().getAll();
        mListData.addAll(list);
        //反转数组
        Collections.reverse(mListData);
        getActivity().runOnUiThread(() -> {
            //更新列表
            adapter.notifyDataSetChanged();
        });
    }


    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @OnClick(R.id.send_message_btn)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_message_btn:
                startActivity(new Intent(getActivity(), SendMessageActivity.class));
                break;
        }
    }


    @Override
    public void OnItemClickListener(View view, int postion) {
        Session session = mListData.get(postion);
        if (session == null) {
            return;
        }
        String targetNumber = session.getCardId();
        Intent intent = new Intent(getActivity(), MessageItemActivity.class);
        intent.putExtra("target", targetNumber);
        startActivity(intent);
    }

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @Override
    public void onMessageReceived(String target) {
        super.onMessageReceived(target);
        getListData();
    }
}


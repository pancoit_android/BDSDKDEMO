package com.pancoit.bdsdkdemo.fragment;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.activity.Command.SelectCommandActivity;
import com.pancoit.bdsdkdemo.base.BaseFragment;
import com.pancoit.bdsdkdemo.constant.Constant;
import com.pancoit.bdsdkdemo.constant.GlobalParams;
import com.pancoit.bdsdkdemo.handler.BeidouSDKHandler;
import com.pancoit.bdsdkdemo.utils.TimeUtil;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.utils.CheckCodeUtil;
import com.pancoit.bdsdklibrary.utils.FormatConversionUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * - @Description:  状态页
 * - @Author:  LXJ
 * - @Time:  2019/11/12 15:53
 */
public class CommandFragment extends BaseFragment {
    @BindView(R.id.box_output_tv)
    TextView boxOutputTv;
    @BindView(R.id.box_output_layout)
    LinearLayout boxOutputLayout;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.choose_command_btn)
    Button chooseCommandBtn;
    @BindView(R.id.send_btn)
    Button sendBtn;
    @BindView(R.id.input_layout)
    RelativeLayout inputLayout;
    @BindView(R.id.instruct)
    EditText instruct;

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    private Unbinder unbinder;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_command;
    }

    @Override
    public void initContentView(View viewContent) {
        unbinder = ButterKnife.bind(this, viewContent);
    }

    @Override
    protected void initView() {
        MainApp.getInstance().instruct = instruct;
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        BeidouSDKHandler.getInstance().addAgentListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BeidouSDKHandler.getInstance().removeAgentListener(this);
    }

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    /**
     * 添加字符串到TextView
     *
     * @param str
     */
    private void addTextToView(final String str) {
        getActivity().runOnUiThread(() -> {
            boxOutputTv.append("\n\n" + TimeUtil.getNowTime());
            boxOutputTv.append("\n" + str);
            scroll2Bottom(scrollView, boxOutputLayout);
        });
    }

    /**
     * 滚动到底部
     *
     * @param scroll Textview
     * @param inner  Layout
     */
    public static void scroll2Bottom(final ScrollView scroll, final View inner) {
        Handler handler = new Handler();
        handler.post(() -> {
            // TODO Auto-generated method stub
            if (scroll == null || inner == null) {
                return;
            }
            // 内层高度超过外层
            int offset = inner.getMeasuredHeight()
                    - scroll.getMeasuredHeight();
            if (offset < 0) {
                System.out.println("定位...");
                offset = 0;
            }
            scroll.scrollTo(0, offset);
        });
    }

    @OnClick({R.id.choose_command_btn, R.id.send_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.choose_command_btn:
                startActivityForResult(new Intent(getContext(), SelectCommandActivity.class),Constant.RequestCode);
                break;
            case R.id.send_btn:
                String str = instruct.getText().toString();
                if (str == ""){
                    return;
                }
                String it = "$" + str + "*"+ CheckCodeUtil.getCheckCode0007(FormatConversionUtil.stringToHex(str));
                BeidouSDKManage.getInstance().sendHexCommand(FormatConversionUtil.stringToHex(it)+"0D0A");
                addTextToView(it);
                instruct.setText("");
                break;
        }
    }

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @Override
    public void onAllCommandPrint(String command) {
        super.onAllCommandPrint(command);
        if (command.length() < 12) return;
        String tag = command.substring(6, 12);
        if (!(
                tag.equals("5A4441")//不打印ZDA
                        || tag.equals("475356")//不打印GSV
                        || tag.equals("474741")//不打印GGA
                        || tag.equals("524D43")//不打印RMC
                        || tag.equals("474C4C")//不打印GLL
                        || tag.equals("475341")//不打印GSA
                        || tag.equals("425349")//不打印BSI0
        )) {
            addTextToView(FormatConversionUtil.hexToString(command));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constant.ResultCode) {
            String str = data.getStringExtra(Constant.instruct);
            instruct.setText(str);
        }
    }
}


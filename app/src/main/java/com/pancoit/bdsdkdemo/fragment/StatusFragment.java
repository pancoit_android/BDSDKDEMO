package com.pancoit.bdsdkdemo.fragment;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.activity.MainActivity;
import com.pancoit.bdsdkdemo.activity.SendMessageActivity;
import com.pancoit.bdsdkdemo.base.BaseFragment;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static java.lang.Thread.sleep;

/**
 * - @Description:  状态页
 * - @Author:  LXJ
 * - @Time:  2019/11/12 15:53
 */
public class StatusFragment extends BaseFragment {
    private Unbinder unbinder;
    //卡号
    @BindView(R.id.card_tv)
    TextView cardTv;
    //频度
    @BindView(R.id.frequency_tv)
    TextView frequencyTv;
    //通信等级
    @BindView(R.id.communication_level_tv)
    TextView communicationLevelTv;
    //电量
    @BindView(R.id.electricity_tv)
    TextView electricityTv;
    //连接/断开盒子按钮
    @BindView(R.id.connect_box_btn)
    Button connectBoxBtn;
    //发送消息按钮
    @BindView(R.id.send_message_btn)
    Button sendMessageBtn;
    //波束1
    @BindView(R.id.signal_0_img)
    ImageView signal0Img;
    //波束2
    @BindView(R.id.signal_1_img)
    ImageView signal1Img;
    //波束3
    @BindView(R.id.signal_2_img)
    ImageView signal2Img;
    //波束4
    @BindView(R.id.signal_3_img)
    ImageView signal3Img;
    //波束5
    @BindView(R.id.signal_4_img)
    ImageView signal4Img;
    //波束6
    @BindView(R.id.signal_5_img)
    ImageView signal5Img;
    //波束7
    @BindView(R.id.signal_6_img)
    ImageView signal6Img;
    //波束8
    @BindView(R.id.signal_7_img)
    ImageView signal7Img;
    //波束9
    @BindView(R.id.signal_8_img)
    ImageView signal8Img;
    //波束10
    @BindView(R.id.signal_9_img)
    ImageView signal9Img;

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/
    private boolean isRefreshing = false;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_status;
    }

    @Override
    public void initContentView(View viewContent) {
        unbinder = ButterKnife.bind(this, viewContent);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void onShow() {
        super.onShow();
        autoRefresh();
    }

    @Override
    public void onHide() {
        super.onHide();
        isRefreshing = false;
    }

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @OnClick({R.id.connect_box_btn, R.id.send_message_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.connect_box_btn:
                //连接蓝牙
                if (TerminalParams.isConnectNormal) {
                    //断开蓝牙连接
                    BeidouSDKManage.getInstance().disConnectDevice();
                } else {
                    checkActivityAttached();
                    ((MainActivity) getActivity()).showBluetoothDialog();
                }
                break;
            case R.id.send_message_btn:
                checkActivityAttached();
                ((MainActivity) getActivity()).changePage(1);
                startActivity(new Intent(getActivity(), SendMessageActivity.class));
                break;
            case R.id.cancel_btn:
                checkActivityAttached();
                ((MainActivity) getActivity()).hideBluetoothDialog();
                break;
        }
    }

    /*******************************************(界面刷新)******************************************************/

    /**
     * 自动刷新
     */
    private void autoRefresh() {
        if (isRefreshing) {
            return;
        }
        isRefreshing = true;
        new Thread(() -> {
            try {
                while (isRefreshing) {
                    Message msg = new Message();
                    msg.what = 1;
                    mHandler.sendMessage(msg);
                    sleep(1000);
                }
            } catch (Exception e) {
                Log.e("InfoMessage", e.toString());
            }
        }).start();
    }

    /**
     * 刷新UI
     */
    private Handler mHandler = new Handler(msg -> {
        switch (msg.what) {
            case 1:
                refreshUiStatus();
                break;
        }
        return false;
    });

    /**
     * 刷新界面
     */
    private void refreshUiStatus() {
        if (TerminalParams.isConnectNormal) {
            connectBoxBtn.setText(getString(R.string.diconnect));
            sendMessageBtn.setVisibility(View.VISIBLE);
            cardTv.setText(getString(R.string.card_number, TerminalParams.cardNumber));
            frequencyTv.setText(getString(R.string.frequency, String.valueOf(TerminalParams.frequency)));
            communicationLevelTv.setText(getString(R.string.communication_level, TerminalParams.communicationLevel));
            electricityTv.setText(getString(R.string.electricity, TerminalParams.electricity));
            signal0Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal1);
            signal1Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal2);
            signal2Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal3);
            signal3Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal4);
            signal4Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal5);
            signal5Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal6);
            signal6Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal7);
            signal7Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal8);
            signal8Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal9);
            signal9Img.setImageResource(R.mipmap.signal_0 + TerminalParams.beidouSignal10);
        } else {
            connectBoxBtn.setText(getString(R.string.connect_box));
            sendMessageBtn.setVisibility(View.GONE);
            cardTv.setText(getString(R.string.card_number, ""));
            frequencyTv.setText(getString(R.string.frequency, ""));
            communicationLevelTv.setText(getString(R.string.communication_level, ""));
            electricityTv.setText(getString(R.string.electricity, ""));
            signal0Img.setImageResource(R.mipmap.signal_0);
            signal1Img.setImageResource(R.mipmap.signal_0);
            signal2Img.setImageResource(R.mipmap.signal_0);
            signal3Img.setImageResource(R.mipmap.signal_0);
            signal4Img.setImageResource(R.mipmap.signal_0);
            signal5Img.setImageResource(R.mipmap.signal_0);
            signal6Img.setImageResource(R.mipmap.signal_0);
            signal7Img.setImageResource(R.mipmap.signal_0);
            signal8Img.setImageResource(R.mipmap.signal_0);
            signal9Img.setImageResource(R.mipmap.signal_0);
        }
    }
}


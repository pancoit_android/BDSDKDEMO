package com.pancoit.bdsdkdemo.constant;

/**
 * - @Description:  常量
 * - @Author:  LXJ
 * - @Time:  2019/9/26 13:53
 */
public class Constant {
    /**
     * 输出格式
     */
    public static String outputFormat = "output_format";

    /**
     * 自检频度
     */
    public static String selfCheckingFrequency = "self_checking_frequency";

    public static int RequestCode = 01;
    public static int ResultCode = 02;

    public static String instruct = "instruct";
}

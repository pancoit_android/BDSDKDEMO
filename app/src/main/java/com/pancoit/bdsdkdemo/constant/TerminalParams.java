package com.pancoit.bdsdkdemo.constant;

/**
 * - @Description:  终端参数
 * - @Author:  LXJ
 * - @Time:  2019/11/13 9:54
 */
public class TerminalParams {
    /**
     * 是否正常连接
     */
    public static boolean isConnectNormal = false;

    /**
     * 北斗卡号
     */
    public static String cardNumber = "";

    /**
     * 蓝牙名称
     */
    public static String blueName = "";

    /**
     * 电量
     */
    public static String electricity = "";

    /**
     * 频度
     */
    public static int frequency = -1;

    /**
     * 当前发送等待时间，0为空闲
     */
    public static int sentWaitSec = 0;

    /**
     * 通信等级
     */
    public static String communicationLevel = "";

    /**
     * 北斗盒子信号
     */
    public static int beidouSignal1 = 0;
    public static int beidouSignal2 = 0;
    public static int beidouSignal3 = 0;
    public static int beidouSignal4 = 0;
    public static int beidouSignal5 = 0;
    public static int beidouSignal6 = 0;
    public static int beidouSignal7 = 0;
    public static int beidouSignal8 = 0;
    public static int beidouSignal9 = 0;
    public static int beidouSignal10 = 0;

    /**
     * SOS中心号码
     */
    public static String sosCenterNumber = "";

    /**
     * SOS频度
     */
    public static int sosFrequency = -1;


    /**
     * SOS内容
     */
    public static String sosContent = "";

    /**
     * 极限追踪中心号码
     */
    public static String limitTrackingCenterNumber = "";


    /**
     * 极限追踪频度
     */
    public static int limitTrackingFrequency = -1;

    /**
     * ok报平安中心号码
     */
    public static String okCenterNumber = "";

    /**
     * ok内容
     */
    public static String okContent = "";

    /**
     * MCU版本
     */
    public static String mcuVersion = "";

    /**
     * 初始化
     */
    public static void initialize() {
        isConnectNormal = false;
        cardNumber = "";
        blueName = "";
        electricity = "";
        frequency = -1;
        sentWaitSec = 0;
        communicationLevel = "";
        beidouSignal1 = 0;
        beidouSignal2 = 0;
        beidouSignal3 = 0;
        beidouSignal4 = 0;
        beidouSignal5 = 0;
        beidouSignal6 = 0;
        beidouSignal7 = 0;
        beidouSignal8 = 0;
        beidouSignal9 = 0;
        beidouSignal10 = 0;
        sosCenterNumber = "";
        sosFrequency = -1;
        sosContent = "";
        limitTrackingCenterNumber = "";
        limitTrackingFrequency = -1;
        okCenterNumber = "";
        okContent = "";
        mcuVersion = "";
    }
}

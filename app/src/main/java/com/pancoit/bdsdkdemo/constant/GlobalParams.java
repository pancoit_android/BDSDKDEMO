package com.pancoit.bdsdkdemo.constant;

/**
 * - @Description:  全局参数
 * - @Author:  LXJ
 * - @Time:  2019/11/13 9:45
 */
public class GlobalParams {
    /**
     * 输出格式
     * 0:十六进制
     * 1：字符串
     */
    public static int outputFormat = 0;

    /**
     * 自检频度(s)
     */
    public static int selfCheckingFrequency = 3;

    public static String AUTO_CONNECT = "PG_AUTO_CONNECT";
    public static String AUTO_CONNECT_NUMBER = "PG_AUTO_CONNECT_NUMBER";
}

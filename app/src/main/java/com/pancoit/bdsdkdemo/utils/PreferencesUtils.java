package com.pancoit.bdsdkdemo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.pancoit.bdsdkdemo.MainApp;

import java.util.List;
import java.util.Map;

/**
 * - @Description: Preferences工具类
 * - @Author:  LXJ
 * - @Time:  2018/8/22 10:02
 */
public class PreferencesUtils {
    /**
     * 用户数据存放文件名
     */
    private static final String USER_INFO = "user_info";

    private static SharedPreferences sharedPreferences = null;

    /**
     * 实例化
     *
     * @param context
     * @return
     */
    public static SharedPreferences getInstance(Context context) {
        if (sharedPreferences == null) {
            synchronized (PreferencesUtils.class) {
                if (sharedPreferences == null) {
                    sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
                }
            }
        }
        return sharedPreferences;
    }

    public static void saveData(Context context, Map<String, Object> list) {
        if (list != null && list.size() > 0) {
            Editor editor = getInstance(context).edit();
            for (String key : list.keySet()) {
                Object value = list.get(key);
                if (value instanceof Integer) {
                    editor.putInt(key, (Integer) value);
                }
                if (value instanceof Long) {
                    editor.putLong(key, (Long) value);
                }
                if (value instanceof Float) {
                    editor.putFloat(key, (Float) value);
                }
                if (value instanceof String) {
                    editor.putString(key, (String) value);
                }
                if (value instanceof Boolean) {
                    editor.putBoolean(key, (Boolean) value);
                }
            }
            editor.commit();
        }
    }

    /**
     * 通过键啊，值啊，传一个就是了
     *
     * @param context
     * @param key
     * @param value
     */
    public static void saveData(Context context, String key, Object value) {
        Editor editor = getInstance(context).edit();
        if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        }
        if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        }
        if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        }
        if (value instanceof String) {
            editor.putString(key, (String) value);
        }
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        }

        editor.commit();
    }

    /**
     * 删除SharedPreferences对应的值
     *
     * @param context
     * @param keys    键值list
     */
    public static void deleteData(Context context, List<String> keys) {
        Editor editor = getInstance(context).edit();
        for (int i = 0; i < keys.size(); i++) {
            if (keys.get(i) != null) {
                editor.remove(keys.get(i));
            }
        }
        editor.commit();
    }

    /**
     * 从SharedPreferences获取对应键值的int值
     *
     * @param key 键值
     * @return
     */
    public static int getInt(String key) {
        SharedPreferences sp = getInstance(MainApp.getInstance());
        int value = sp.getInt(key, 0);
        return value;
    }

    /**
     * 从SharedPreferences获取对应键值的String值
     *
     * @param key 键值
     * @return
     */
    public static String getString(String key) {
        SharedPreferences sp = getInstance(MainApp.getInstance());
        String value = sp.getString(key, "");
        return value;
    }


    /**
     * 存入对应的键值对
     *
     * @param context
     * @param key     键值
     * @param value   对应的int值
     */
    public synchronized static void setInt(Context context, String key, int value) {
        if (key == null) {
            throw new NullPointerException();
        }
        Editor editor = getInstance(context).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * 存入对应的键值对
     *
     * @param context
     * @param key     键值
     * @param value   对应的String值
     */
    public static void setString(Context context, String key, String value) {
        if (key == null || value == null) {
            throw new NullPointerException();
        }
        Editor editor = getInstance(context).edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * 存入对应的键值对
     *
     * @param context
     * @param key     键值
     * @param value   对应的boolean值
     */
    public static void setBoolean(Context context, String key, boolean value) {
        if (key == null) {
            throw new NullPointerException();
        }
        Editor editor = getInstance(context).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }


    /**
     * 从SharedPreferences获取对应键值的Boolean值
     *
     * @param context
     * @param key     键值
     * @return
     */
    public static boolean getBoolean(Context context, String key, boolean defvalue) {
        SharedPreferences sp = getInstance(context);
        boolean value = sp.getBoolean(key, defvalue);
        return value;
    }


    private PreferencesUtils() {
        throw new AssertionError();
    }


    public static Editor getEditor() {
        return getInstance(MainApp.getInstance()).edit();
    }
}

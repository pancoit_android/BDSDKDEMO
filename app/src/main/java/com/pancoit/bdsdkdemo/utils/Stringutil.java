package com.pancoit.bdsdkdemo.utils;

/**
 * - @Description:  字符串工具类
 * - @Author:  LXJ
 * - @Time:  2019/11/15 9:38
 */
public class Stringutil {
    /**
     * 从左往右取第一个不为零的数开始取
     *
     * @param str
     * @return
     */
    public static String subStringNotZero(String str) {
        boolean isZero = false;
        StringBuffer strBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            if (!isZero) {
                if (!str.substring(i, i + 1).equals("0")) {
                    strBuffer.append(str.substring(i, i + 1));
                    isZero = true;
                }
                continue;
            }
            strBuffer.append(str.substring(i, i + 1));
        }
        return strBuffer.toString();
    }
}

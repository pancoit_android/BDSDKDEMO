package com.pancoit.bdsdkdemo.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * - @Description:  时间工具类
 * - @Author:  LXJ
 * - @Time:  2019/2/26 10:22
 */
public class TimeUtil {

    /**
     * 获取当前时间 yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static String getNowTime() {
        String time;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
        time = sdf.format(new Date());
        return time;
    }

    /**
     * 取得当前时间戳（精确到秒）
     *
     * @return
     */
    public static String getTimeStamp() {
        long time = System.currentTimeMillis();
        String timeStamp = String.valueOf(time / 1000);
        return timeStamp;
    }

    /**
     * 获取当前时间 年
     *
     * @return
     */
    public static String getNowYear() {
        String time;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy", Locale.CHINA);
        time = sdf.format(new Date());
        return time;
    }

    /**
     * 获取当前时间 月
     *
     * @return
     */
    public static String getMonth() {
        String time;
        SimpleDateFormat sdf = new SimpleDateFormat("MM", Locale.CHINA);
        time = sdf.format(new Date());
        return time;
    }

    /**
     * 获取目标时间的年或月或日
     *
     * @param targetTime 目标时间
     * @param type       年或者其他，一定要传Calendar类下的
     * @return
     */
    public static String getTargetTime(String targetTime, int type) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
        try {
            Date dt = df.parse(targetTime);
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            int time = c.get(type);
            if (type == Calendar.YEAR) {
                return String.valueOf(time);
            } else if (type == Calendar.MONTH) {
                return (time + 1) < 10 ? "0" + (time + 1) : String.valueOf(time + 1);
            } else {
                return time < 10 ? "0" + time : String.valueOf(time);
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 日期比较
     *
     * @param date1 日期1
     * @param date2 日期2
     * @return date1<date2:1 date1>date2：-1   date1=date2:0
     */
    public static int compareDate(String date1, String date2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date dt1 = df.parse(date1);
            Date dt2 = df.parse(date2);
            long startTime = dt1.getTime();
            long compareTime = dt2.getTime();
            if (startTime <= compareTime) {
                return 1;
            } else {
                if ((startTime - compareTime) <= 15 * 60000) {
                    return 1;
                } else {
                    return -1;
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取指定增加毫秒时间
     *
     * @param time 指定时间 2018-12-12 12：00：00
     * @param i    毫秒数
     * @return
     */
    public static String getDesignateAddTime(String time, long i) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sdf.parse(time);
            Date afterDate = new Date(date.getTime() + i);
            return sdf.format(afterDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取指定减少毫秒时间
     *
     * @param time 指定时间 2018-12-12 12：00：00
     * @param i    毫秒数
     * @return
     */
    public static String getDesignateLessen(String time, long i) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sdf.parse(time);
            Date afterDate = new Date(date.getTime() - i);
            return sdf.format(afterDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 时间戳转换成日期格式字符串
     *
     * @param seconds 精确到秒的时间戳字符串
     * @param format
     * @return
     */
    public static String timeStampToDate(String seconds, String format) {
        if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
            return "";
        }
        if (format == null || format.isEmpty()) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (seconds.length() == 10) {
            return sdf.format(new Date(Long.valueOf(seconds + "000")));
        } else {
            return sdf.format(new Date(Long.valueOf(seconds)));
        }
    }

    /**
     * 日期格式字符串转换成时间戳
     *
     * @param dateStr 字符串日期
     * @param format  如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String dateToTimeStamp(String dateStr, String format) {
        try {
            if (format == null || format.isEmpty()) {
                format = "yyyy-MM-dd HH:mm:ss";
            }
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(dateStr).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String result = format.format(today);
        return result;
    }

    /**
     * 获取未来 第 past 天的日期
     *
     * @param feture
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getFetureDate(int feture) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + feture);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String result = format.format(today);
        Log.e(null, result);
        return result;
    }

    /**
     * 将毫秒转换为日时分秒
     *
     * @param second
     * @return
     */
    public static String secondToTime(long second) {
        //转换天数
        long days = second / 86400;
        //剩余秒数
        second = second % 86400;
        //转换小时数
        long hours = second / 3600;
        //剩余秒数
        second = second % 3600;
        //转换分钟
        long minutes = second / 60;
        //剩余秒数
        second = second % 60;
        if (days > 0) {
            return days + "天，" + (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (second < 10 ? "0" + second : second);
        } else if (hours > 0) {
            return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (second < 10 ? "0" + second : second);
        } else if (minutes > 0) {
            return (minutes < 10 ? "0" + minutes : minutes) + ":" + (second < 10 ? "0" + second : second);
        } else {
            return (second < 10 ? "0" + second : second) + "s";
        }
    }

    /**
     * 天-》时间戳(秒)
     *
     * @param day 天数
     * @return
     */
    public static long dayToTimestamp(int day) {
        long sec = 86400;
        long timestamp = day * sec;
        return timestamp;
    }
}

package com.pancoit.bdsdkdemo.utils;

import android.content.Context;
import android.util.Log;

import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.utils.CheckCodeUtil;
import com.pancoit.bdsdklibrary.utils.FormatConversionUtil;

import java.util.Timer;
import java.util.TimerTask;

public class TestSendMessageTool {
    private static TestSendMessageTool testSendMessageTool;
    public boolean isStart = false;

    //电文形式
    public int mode = 3;
    // 协议
    public int protocol = 1;
    //频度
    public int frequency = 33;

    public String number = "15950044";
    public String cable = "";

    private Timer timer;

    public static TestSendMessageTool getInstance() {
        if (testSendMessageTool == null) {
            testSendMessageTool = new TestSendMessageTool();
        }
        return testSendMessageTool;
    }

    public void sendMessage(String text){
        String str = getCommand(text);
        String it = "$" + str + "*"+ CheckCodeUtil.getCheckCode0007(FormatConversionUtil.stringToHex(str));
        Log.e("sendMessage", it);
        BeidouSDKManage.getInstance().sendHexCommand(FormatConversionUtil.stringToHex(it)+"0D0A");
    }

    public void startSendMessage(String text, int fre){
        frequency = fre;
        cable = text;
        isStart = true;
        TimerTask sosTask = new TimerTask() {
            @Override
            public void run() {
                if (!isStart) return;
                sendMessage(cable);
            }
        };

        timer = new Timer();
        timer.schedule(sosTask, 1,fre*1000);
    }

    public void stop(){
        isStart = false;
        if (timer != null){
            timer.cancel();
            timer = null;
        }
    }

    public String getCommand(String content){
        if (protocol == 1){
            int m = mode-1;
            return "CCTXA," + number + ",1," + m + "," + content ;
        }else if (protocol == 2) {
            return "CCMSG," + number + ",2," + mode + "," + content;
        }else if (protocol == 3){
            return "CCTCQ,"+number + ",1,1," + mode + ","+ content + ",0";
        }else {
            return "";
        }
    }
}

package com.pancoit.bdsdkdemo.view;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.Nullable;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.constant.Constant;
import com.pancoit.bdsdkdemo.constant.GlobalParams;
import com.pancoit.bdsdkdemo.constant.TerminalParams;
import com.pancoit.bdsdkdemo.utils.PreferencesUtils;
import com.pancoit.bdsdklibrary.constant.BeidouSDKParam;
import com.pancoit.bdsdklibrary.manage.BeidouSDKManage;
import com.pancoit.bdsdklibrary.protocol.Protocal21Write;


/**
 * - @Description:  蓝牙连接弹框
 * - @Author:  LXJ
 * - @Time:  2018/12/6 18:20
 */
public class CommandSettingDialog extends DialogFragment implements View.OnClickListener {
    private EditText frequencyEdit;
    private Spinner outputSp;

    public CommandSettingDialog() {

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            dialog.getWindow().setLayout((int) (dm.widthPixels * 0.9), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewContent = inflater.inflate(R.layout.dialog_command_setting, container, false);
        //去掉dialog默认标题
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        //初始化控件
        frequencyEdit = viewContent.findViewById(R.id.frequency_edit);
        outputSp = viewContent.findViewById(R.id.output_format_sp);
        Button cancelBtn = viewContent.findViewById(R.id.cancel_btn);
        Button confirmBtn = viewContent.findViewById(R.id.confirm_btn);
        cancelBtn.setOnClickListener(this);
        confirmBtn.setOnClickListener(this);
        outputSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String[] languages = getResources().getStringArray(R.array.output_format);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        initViewData();
        return viewContent;
    }

    /**
     * 初始化控件数据
     */
    private void initViewData() {
        frequencyEdit.setText(String.valueOf(GlobalParams.selfCheckingFrequency));
        outputSp.setSelection(GlobalParams.outputFormat);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_btn:
                dismiss();
                break;
            case R.id.confirm_btn:
                //自检频度
                String frq = frequencyEdit.getText().toString().trim();
                if (TextUtils.isEmpty(frq)) {
                    MainApp.getInstance().showMsg("请输入自检频度");
                    return;
                }
                GlobalParams.selfCheckingFrequency = Integer.parseInt(frq);
                PreferencesUtils.saveData(MainApp.getInstance(), Constant.selfCheckingFrequency, GlobalParams.selfCheckingFrequency);
                //输出格式
                GlobalParams.outputFormat = outputSp.getSelectedItemPosition();
                PreferencesUtils.saveData(MainApp.getInstance(), Constant.outputFormat, GlobalParams.outputFormat);
                if (TerminalParams.isConnectNormal) {
                    if (BeidouSDKParam.boxVersion == 2) {
                        BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCZDC(frq));
                    }
                }
                dismiss();
                break;
        }
    }
}

package com.pancoit.bdsdkdemo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

/**
 * - @Description:  自定义ViewPager
 * - @Author:  LXJ
 * - @Time:  2019/11/12 14:53
 */
public class FragmentViewPager extends ViewPager {
    /**
     * 设置其是否能滑动换页
     * false 不能换页， true 可以滑动换页
     */
    private boolean isCanScroll = false;

    public FragmentViewPager(@NonNull Context context) {
        super(context);
    }

    public FragmentViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setScanScroll(boolean isCanScroll) {
        this.isCanScroll = isCanScroll;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isCanScroll && super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return isCanScroll && super.onTouchEvent(ev);

    }
}

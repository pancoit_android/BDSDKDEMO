package com.pancoit.bdsdkdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.entity.UserMessage;
import com.pancoit.bdsdkdemo.entity.emums.MessageIOType;

import java.util.List;

/**
 * - @Description:  聊天列表适配器
 * - @Author:  LXJ
 * - @Time:  2019/11/15 10:26
 */
public class MessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<UserMessage> mList;
    private LayoutInflater mLayoutInflater;

    public MessageListAdapter(Context context, List<UserMessage> mList) {
        this.mContext = context;
        this.mList = mList;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getMessageType();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MessageIOType.IN.getValue()) {
            return new ReceiveMessageViewHolder(mLayoutInflater.inflate(R.layout.message_list_item_receive, parent, false));
        } else {
            return new SendMessageViewHolder(mLayoutInflater.inflate(R.layout.message_list_item_send, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UserMessage userMessage = mList.get(position);
        if (holder instanceof ReceiveMessageViewHolder) {
            //时间
            ((ReceiveMessageViewHolder) holder).timeTv.setText(userMessage.getTime());
            //内容
            ((ReceiveMessageViewHolder) holder).contentTv.setText(userMessage.getContent());
            //头像
            ((ReceiveMessageViewHolder) holder).headImg.setImageResource(R.mipmap.box_img);
        } else {
            //时间
            ((SendMessageViewHolder) holder).timeTv.setText(userMessage.getTime());
            //内容
            ((SendMessageViewHolder) holder).contentTv.setText(userMessage.getContent());
            //头像
            ((SendMessageViewHolder) holder).headImg.setImageResource(R.mipmap.box_img);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    /**
     * 发出
     */
    class SendMessageViewHolder extends RecyclerView.ViewHolder {
        private TextView timeTv;
        private TextView contentTv;
        private ImageView headImg;

        public SendMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            timeTv = itemView.findViewById(R.id.time_tv);
            contentTv = itemView.findViewById(R.id.content_tv);
            headImg = itemView.findViewById(R.id.head_img);
        }
    }

    /**
     * 收到
     */
    class ReceiveMessageViewHolder extends RecyclerView.ViewHolder {
        private TextView timeTv;
        private TextView contentTv;
        private ImageView headImg;

        public ReceiveMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            timeTv = itemView.findViewById(R.id.time_tv);
            contentTv = itemView.findViewById(R.id.content_tv);
            headImg = itemView.findViewById(R.id.head_img);
        }
    }
}

package com.pancoit.bdsdkdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.entity.Session;

import java.util.List;

/**
 * - @Description:  会话列表
 * - @Author:  LXJ
 * - @Time:  2019/11/14 9:46
 */
public class SessionRecylerviwAdapter extends RecyclerView.Adapter<SessionRecylerviwAdapter.MyViewHolder> {
    private Context mContext;
    private List<Session> mList;
    private OnItemClickListener mOnItemClickListener;

    public SessionRecylerviwAdapter(Context context, List<Session> list, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mList = list;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.session_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view, mOnItemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Session session = mList.get(position);
        //名称
        if (session.getName() == null) {
            holder.nameTv.setText(session.getCardId());
        } else {
            holder.nameTv.setText(session.getName());
        }
        //内容
        holder.contentTv.setText(session.getContent());
        //时间
        holder.timeTv.setText(session.getTime());
        //头像
        holder.headImg.setImageResource(R.mipmap.box_img);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int postion);
    }

    /**
     * 刷新整个列表
     *
     * @param list
     */
    public void upDateList(List<Session> list) {
        mList = list;
        notifyDataSetChanged();
    }

    /**
     * 更新某个子项
     *
     * @param session
     * @param pos
     */
    public void upDateListItem(Session session, int pos) {
        mList.set(pos, session);
        notifyItemChanged(pos);
    }

    /**
     * 添加子项
     *
     * @param session
     */
    public void addListItem(Session session) {
        mList.add(session);
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //整个布局
        private RelativeLayout itemLayout;
        //头像
        private ImageView headImg;
        //名称
        private TextView nameTv;
        //内容
        private TextView contentTv;
        //时间
        private TextView timeTv;
        //点击事件
        OnItemClickListener mOnItemClickListener;


        public MyViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            mOnItemClickListener = onItemClickListener;
            itemLayout = itemView.findViewById(R.id.item_layout);
            headImg = itemView.findViewById(R.id.head_img);
            nameTv = itemView.findViewById(R.id.name_tv);
            contentTv = itemView.findViewById(R.id.content_tv);
            timeTv = itemView.findViewById(R.id.time_tv);
            itemLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mOnItemClickListener.OnItemClickListener(view, getLayoutPosition());
        }
    }
}

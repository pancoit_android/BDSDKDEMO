package com.pancoit.bdsdkdemo.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.activity.Command.SelectCommandActivity;

import java.util.List;
import java.util.Map;

public class SelectCommandRecylerViewAdapter extends RecyclerView.Adapter<SelectCommandRecylerViewAdapter.ViewHolder> {
    OnItemClickListener onItemClickListener;
    List<Map<String,Object>> datas;
    public SelectCommandRecylerViewAdapter(List<Map<String,Object>> d,OnItemClickListener Listener){
        datas = d;
        onItemClickListener = Listener;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_command_list,parent,false);
        return new ViewHolder(view,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        Map<String,Object> data = datas.get(position);
        holder.textView.setText((String)data.get(SelectCommandActivity.key_title));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public interface OnItemClickListener {
        void OnItemClickListener(int postion);
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textView;
        LinearLayout itemLayout;
        OnItemClickListener onItemClickListener;
        public ViewHolder(@NonNull View itemView, OnItemClickListener Listener) {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.item_layout);
            textView = itemView.findViewById(R.id.title);
            onItemClickListener = Listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            onItemClickListener.OnItemClickListener(position);
        }
    }

}

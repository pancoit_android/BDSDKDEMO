package com.pancoit.bdsdkdemo.adapter;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pancoit.bdsdkdemo.base.BaseFragment;

import java.util.List;

/**
 * - @Description:  Fragment页面适配器
 * - @Author:  LXJ
 * - @Time:  2018/12/3 13:59
 */

public class AppFragmentPageAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> mList;

    public AppFragmentPageAdapter(@NonNull FragmentManager fm, int behavior, List<BaseFragment> mList) {
        super(fm, behavior);
        this.mList = mList;
    }

    @Override
    public Fragment getItem(int position) {
        if (null == mList || position < 0 || position >= mList.size()) {
            return null;
        }
        return mList.get(position);
    }

    @Override
    public int getCount() {
        if (null == mList) {
            return 0;
        }
        return mList.size();
    }
}

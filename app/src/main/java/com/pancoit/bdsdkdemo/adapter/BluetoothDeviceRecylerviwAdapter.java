package com.pancoit.bdsdkdemo.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.pancoit.bdsdkdemo.R;

import java.util.List;


/**
 * - @Description:  蓝牙设备列表适配器
 * - @Author:  LXJ
 * - @Time:  2018/12/6 18:29
 */

public class BluetoothDeviceRecylerviwAdapter extends RecyclerView.Adapter<BluetoothDeviceRecylerviwAdapter.MyViewHolder> {
    private Context mContext;
    private List<BluetoothDevice> mList;
    private OnItemClickListener mOnItemClickListener;

    public BluetoothDeviceRecylerviwAdapter(Context context, List<BluetoothDevice> list, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mList = list;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.bluetooth_device_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view, mOnItemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final BluetoothDevice device = mList.get(position);
        if (position % 2 == 0) {
            holder.itemLayout.setBackground(mContext.getResources().getDrawable(R.drawable.cilckable_white));
        } else {
            holder.itemLayout.setBackground(mContext.getResources().getDrawable(R.drawable.cilckable_gray));
        }
        holder.deviceName.setText(device.getName());
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.OnItemClickListener(view,position,device);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int postion, BluetoothDevice device);
    }

    /**
     * 刷新整个列表
     *
     * @param list
     */
    public void upDateList(List<BluetoothDevice> list) {
        mList = list;
        notifyDataSetChanged();
    }

    /**
     * 更新某个子项
     *
     * @param device
     * @param pos
     */
    public void upDateListItem(BluetoothDevice device, int pos) {
        mList.set(pos, device);
        notifyItemChanged(pos);
    }

    /**
     * 添加设备
     *
     * @param device
     */
    public void addListItem(BluetoothDevice device) {
        mList.add(device);
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        //整个布局
        private RelativeLayout itemLayout;
        //蓝牙名称
        private TextView deviceName;
        OnItemClickListener mOnItemClickListener;


        public MyViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            mOnItemClickListener = onItemClickListener;
            itemLayout = itemView.findViewById(R.id.item_layout);
            deviceName = itemView.findViewById(R.id.device_name);
        }
    }
}

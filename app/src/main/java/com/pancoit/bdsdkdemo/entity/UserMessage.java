package com.pancoit.bdsdkdemo.entity;

/**
 * - @Description:  用户消息
 * - @Author:  LXJ
 * - @Time:  2019/11/14 14:28
 */
public class UserMessage {
    //id
    private int id;
    //内容
    private String content;
    //时间
    private String time;
    //发送方号码
    private String fromNumber;
    //接收方号码
    private String toNumber;
    //消息类型
    private int messageType;
    //发送状态(默认未知)
    private int status = 2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserMessage{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                ", fromNumber='" + fromNumber + '\'' +
                ", toNumber='" + toNumber + '\'' +
                ", messageType=" + messageType +
                ", status=" + status +
                '}';
    }
}

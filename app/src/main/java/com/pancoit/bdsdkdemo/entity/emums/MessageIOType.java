package com.pancoit.bdsdkdemo.entity.emums;

import android.text.TextUtils;

/**
 * - @Description:  消息收发类型
 * - @Author:  LXJ
 * - @Time:  2019/11/15 10:41
 */
public enum MessageIOType {
    OUT(0, "发出"),
    IN(1, "收到");

    //值
    private int value;
    //名称
    private String name;

    MessageIOType(int v, String n) {
        this.value = v;
        this.name = n;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static MessageIOType get(String n) {
        if (TextUtils.isEmpty(n)) {
            return OUT;
        } else if (n.equals(OUT.getName())) {
            return OUT;
        } else if (n.equals(IN.getName())) {
            return IN;
        } else {
            return OUT;
        }
    }

    public static MessageIOType get(int v) {
        if (v == OUT.getValue()) {
            return OUT;
        } else if (v == IN.getValue()) {
            return IN;
        } else {
            return OUT;
        }
    }
}

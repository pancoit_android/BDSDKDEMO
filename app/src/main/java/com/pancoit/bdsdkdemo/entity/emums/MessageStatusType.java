package com.pancoit.bdsdkdemo.entity.emums;

import android.text.TextUtils;

/**
 * - @Description:  消息状态类型
 * - @Author:  LXJ
 * - @Time:  2019/11/15 10:41
 */
public enum MessageStatusType {
    Succeed(0, "成功"),
    Faild(1, "失败"),
    Unknown(2, "未知");

    //值
    private int value;
    //名称
    private String name;

    MessageStatusType(int v, String n) {
        this.value = v;
        this.name = n;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static MessageStatusType get(String n) {
        if (TextUtils.isEmpty(n)) {
            return Unknown;
        } else if (n.equals(Succeed.getName())) {
            return Succeed;
        } else if (n.equals(Faild.getName())) {
            return Faild;
        } else {
            return Unknown;
        }
    }

    public static MessageStatusType get(int v) {
        if (v == Succeed.getValue()) {
            return Succeed;
        } else if (v == Faild.getValue()) {
            return Faild;
        } else {
            return Unknown;
        }
    }
}

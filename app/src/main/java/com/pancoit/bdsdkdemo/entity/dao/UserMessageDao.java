package com.pancoit.bdsdkdemo.entity.dao;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.entity.UserMessage;
import com.pancoit.bdsdkdemo.entity.emums.MessageIOType;

import java.util.List;

/**
 * - @Description:  用户消息数据库管理类
 * - @Author:  LXJ
 * - @Time:  2019/11/14 17:52
 */
public class UserMessageDao {
    private static UserMessageDao userMessageDao;

    public static UserMessageDao getInstance() {
        if (userMessageDao == null) {
            userMessageDao = new UserMessageDao();
            MainApp.getInstance().getDb().checkTableExist(UserMessage.class);
        }
        return userMessageDao;
    }

    /**
     * 获取所有数据
     *
     * @return
     */
    public List<UserMessage> getAll() {
        List<UserMessage> userMessageList = MainApp.getInstance().getDb().findAllByWhere(UserMessage.class, "1=1 order by id");
        return userMessageList;
    }

    /**
     * 通过目标号码获取数据
     *
     * @return
     */
    public List<UserMessage> getForTargetNumber(String target) {
        List<UserMessage> userMessageList = MainApp.getInstance().getDb().findAllByWhere(UserMessage.class, "toNumber='"  + target + "' or fromNumber='" + target+"'");
        return userMessageList;
    }

    /**
     * 保存一条数据
     *
     * @param userMessage
     * @return
     */
    public UserMessage save(UserMessage userMessage) {
        MainApp.getInstance().getDb().saveBindId(userMessage);
        return userMessage;
    }

    /**
     * 保存一条数据
     *
     * @param fromNumber 发出号码
     * @param toNumber   接收号码
     * @param time       时间
     * @param content    内容
     * @return
     */
    public UserMessage save(String fromNumber, String toNumber, String time, String content) {
        UserMessage userMessage = new UserMessage();
        userMessage.setFromNumber(fromNumber);
        userMessage.setToNumber(toNumber);
        userMessage.setTime(time);
        userMessage.setContent(content);
        userMessage.setMessageType(MessageIOType.IN.getValue());
        MainApp.getInstance().getDb().saveBindId(userMessage);
        return userMessage;
    }

    /**
     * 更新用户消息
     *
     * @param userMessage
     */
    public void update(UserMessage userMessage) {
        MainApp.getInstance().getDb().update(userMessage);
    }
}

package com.pancoit.bdsdkdemo.entity.dao;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.entity.Session;

import java.util.List;

/**
 * - @Description:  会话数据库管理类
 * - @Author:  LXJ
 * - @Time:  2019/11/14 16:41
 */
public class SessionDao {
    private static SessionDao sessionDao;

    public static SessionDao getInstance() {
        if (sessionDao == null) {
            sessionDao = new SessionDao();
            MainApp.getInstance().getDb().checkTableExist(Session.class);
        }
        return sessionDao;
    }

    /**
     * 获取所有数据
     *
     * @return
     */
    public List<Session> getAll() {
        List<Session> sessionList = MainApp.getInstance().getDb().findAllByWhere(Session.class, "1=1 order by id");
        return sessionList;
    }

    /**
     * 获取某号码
     *
     * @return
     */
    public Session getFromCardId(String cardId) {
        List<Session> sessionList = MainApp.getInstance().getDb().findAllByWhere(Session.class, "1=1 and cardId='" + cardId+"'");
        if (sessionList != null && sessionList.size() > 0) {
            return sessionList.get(0);
        }
        return null;
    }

    /**
     * 保存一条数据
     *
     * @param fromNumber
     * @param time
     * @param content
     * @return
     */
    public Session save(String fromNumber, String time, String content) {
        Session session = getFromCardId(fromNumber);
        if (session == null) {
            session = new Session();
            session.setCardId(fromNumber);
            session.setTime(time);
            session.setContent(content);
            MainApp.getInstance().getDb().saveBindId(session);
        } else {
            session.setContent(content);
            session.setTime(time);
            MainApp.getInstance().getDb().update(session);
        }
        return session;
    }
}

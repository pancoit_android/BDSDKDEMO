package com.pancoit.bdsdkdemo.base;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pancoit.bdsdkdemo.MainApp;
import com.pancoit.bdsdkdemo.R;
import com.pancoit.bdsdkdemo.interfaces.IAgentListener;

/**
 * - @Description:  Activity的基类
 * - @Author:  LXJ
 * - @Time:  2019/11/12 15:09
 */
public abstract class BaseActivity extends AppCompatActivity implements IAgentListener, View.OnClickListener {
    private TextView titleTv;
    private ImageView arrowImg;

    /**
     * 获取当前context
     *
     * @return
     */
    public Activity getActivity() {
        return this;
    }

    /**
     * 获取布局
     *
     * @return
     */
    public abstract int getLayoutId();

    /**
     * 初始化contentView
     *
     * @param savedInstanceState
     */
    public abstract void initContentView(Bundle savedInstanceState);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //android6.0以后可以对状态栏文字颜色进行修改
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        setContentView(getLayoutId());
        initContentView(savedInstanceState);
        titleTv = findViewById(R.id.title_tv);
        arrowImg = findViewById(R.id.arrow_img);
        arrowImg.setOnClickListener(this);
        initView();
        initEvent();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApp.getInstance().mActivity = this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getActivity().finish();
    }

    /**
     * 初始化控件
     */
    protected abstract void initView();

    /**
     * 初始化事件
     */
    protected abstract void initEvent();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 是否显示标题栏
     */
    protected void initTitleBar(boolean isShowBackButton, String title) {
        titleTv.setText(title);
        arrowImg.setVisibility(isShowBackButton ? View.VISIBLE : View.GONE);
    }

    /**
     * 隐藏Activity所在Window的系统软键盘
     */
    public void hideInputKeybord() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != inputMethodManager) {
            inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    /**
     * 隐藏View所在Window的系统软键盘
     *
     * @param view 任意画面Visible控件
     */
    public void hideInputKeybord(final View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != inputMethodManager) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * 显示系统软键盘
     */
    public void showInputKeybord(final View view) {
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (null != imm) {
                    view.requestFocus();
                    imm.showSoftInput(view, 0);
                }
            }
        }, 100);
    }

    /**
     * 隐藏或显示状态栏
     *
     * @param isShown true表示显示,false表示隐藏
     */
    protected void setStatusBar(boolean isShown) {
        if (isShown) {
            WindowManager.LayoutParams attrs = getWindow().getAttributes();
            attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
            getWindow().setAttributes(attrs);
        } else {
            WindowManager.LayoutParams attrs = getWindow().getAttributes();
            attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            getWindow().setAttributes(attrs);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.arrow_img:
                hideInputKeybord();
                getActivity().finish();
                break;
        }
    }

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @Override
    public void onConnectSuccess() {

    }

    @Override
    public void onDisconnectSuccess() {

    }

    @Override
    public void onMessageReceived(String target) {

    }

    @Override
    public void onCommandFeedback(String target) {

    }

    @Override
    public void onAllCommandPrint(String command) {

    }

    @Override
    public void onSOSInfoReceived(String centerNumber, int frequency, String content) {

    }

    @Override
    public void onLimitTrackingInfoReceived(String centerNumber, int frequency) {

    }

    @Override
    public void onOKInfoReceived(String centerNumber, String content) {

    }

    @Override
    public void onVersionInfoReceived(String version) {

    }
}

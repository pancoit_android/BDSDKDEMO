package com.pancoit.bdsdkdemo.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pancoit.bdsdkdemo.interfaces.IAgentListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * - @Description:  Ftagment基类
 * - @Author:  LXJ
 * - @Time:  2019/11/12 15:36
 */
public abstract class BaseFragment extends Fragment implements IAgentListener {
    //我们自己的Fragment需要缓存视图
    protected View viewContent;
    //是否初始化视图
    private boolean isInit;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (viewContent == null) {
            viewContent = inflater.inflate(getLayoutId(), container, false);
            initContentView(viewContent);
        }
        //判断Fragment对应的Activity是否存在这个视图
        ViewGroup parent = (ViewGroup) viewContent.getParent();
        if (parent != null) {
            //如果存在,那么我就干掉,重写添加,这样的方式我们就可以缓存视图
            parent.removeView(viewContent);
        }
        return viewContent;
    }

    /**
     * 获取布局
     *
     * @return
     */
    public abstract int getLayoutId();

    /**
     * 初始化contentview
     *
     * @param viewContent
     */
    public abstract void initContentView(View viewContent);

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!isInit) {
            this.isInit = true;
            initView();
            initEvent();
            initData();
        }
    }

    /**
     * 初始化控件
     */
    protected abstract void initView();

    /**
     * 初始化事件
     */
    protected abstract void initEvent();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    @Override
    public void onStart() {
        super.onStart();
        refreshView();
    }

    @Override
    public void onResume() {
        super.onResume();
        onShow();
    }


    @Override
    public void onPause() {
        super.onPause();
        onHide();
    }

    /**
     * 显示
     */
    public void onShow() {
    }

    /**
     * 隐藏
     */
    public void onHide() {
    }

    /**
     * 刷新
     */
    public void refreshView() {
    }

    /**
     * 检查activity连接情况
     */
    protected void checkActivityAttached() {
        if (getActivity() == null) {
            throw new ActivityNotAttachedException();
        }
    }

    /**
     * 检查Activity连接情况
     */
    protected static class ActivityNotAttachedException extends RuntimeException {
        public ActivityNotAttachedException() {
            super("Fragment has disconnected from Activity ! - -.");
        }
    }

    /**
     * string中添加参数
     * @param id
     * @param text
     * @return
     */
    public String getString(int id,String text){
        return String.format(getString(id),text);
    }

    /*******************************************(只是单纯的强迫症想分割一下)******************************************************/

    @Override
    public void onConnectSuccess() {

    }

    @Override
    public void onDisconnectSuccess() {

    }

    @Override
    public void onMessageReceived(String target) {

    }

    @Override
    public void onCommandFeedback(String target) {

    }

    @Override
    public void onAllCommandPrint(String command) {

    }

    @Override
    public void onSOSInfoReceived(String centerNumber, int frequency, String content) {

    }

    @Override
    public void onLimitTrackingInfoReceived(String centerNumber, int frequency) {

    }

    @Override
    public void onOKInfoReceived(String centerNumber, String content) {

    }

    @Override
    public void onVersionInfoReceived(String version) {

    }
}

##  北斗SDK使用说明

### 一、配置工程

#### 1.添加aar文件

<br>将bdsdklibrary.aar文件复制到工程的libs目录下:
<br>![输入图片描述](http://pic.pancoit.com/PanGuYum/tdwt/image/1577412696995-2008713879.png)
<br>并在app下的build.gradle中的dependencies中添加依赖：
```
//北斗应急终端SDK  
implementation(name: 'bdsdklibrary_1.0', ext: 'aar')
```
#### 2.初始化(注册)

<br>推荐在工程的Application中的onCreate()中初始化：
```
//注册北斗应急终端SDK  
BeidouSDKManage.register(this);
```
#### 3.终端数据监听

<br>使用 BeidouSDKManage.getInstance().setProtocolListener()方法可添加以下两种监听器，用于监听终端返回数据:
①I40ProtocolListener

北斗4.0协议监听器，一般是一代盒子PD01使用。

②I21ProtocolListener

北斗2.1协议监听器，一般是PD02/PD03/PD11/PD13，设备使用。

### 二、蓝牙管理模块
#### 1.连接蓝牙
<br>过搜索盒子蓝牙获取到盒子的BluetoothDevice后，调用以下代码连接终端：

```java
//连接蓝牙  
BeidouSDKManage.getInstance().connectDevice(device.getAddress());
```
<br>连接成功后，会执行监听器中的onConnectBleSuccess()方法。
#### 2.断开蓝牙

<br>调用以下代码与终端断开连接：

```java
//断开蓝牙连接  
BeidouSDKManage.getInstance().disConnectDevice();
```
<br>断开连接成功后会执行监听器中的onDisconnectBleSucces()方法。

#### 3.自动重连功能
<br>通过以下代码可以打开或者关闭自动重连功能：

```java
//自动重连,true:打开;flase:关闭;默认关闭
BeidouSDKParam.isAutoConnect=true;
```
#### 4.写入蓝牙数据
<br>为了方便，SDK提供了三种发送方法：

<br>①sendBytesCommand(byte[] bytes)
发送字节数组，使用方法如下：

```java
//发送字节数组指令  
BeidouSDKManage.getInstance().sendBytesCommand(commandBytes);
```
<br>②sendStringCommand(String string)
发送字符串，使用方法如下：

```java
//发送字符串指令，版本查询  
BeidouSDKManage.getInstance().sendStringCommand("$CCVRQ,*79\r\n");
```
<br>③sendHexCommand(String hex)
发送十六进制字符串，使用方法如下：

```java
//发送十六进制指令，版本查询  
BeidouSDKManage.getInstance().sendHexCommand("2443435652512C2A37390D0A");
```
#### 5.接收数据
<br>收到终端数据后，会通过监听器下的onAllCommandReceived(String hex)方法返回十六进制数据，如需其他格式可自行转换。

### 三、终端管理模块
#### 1.盒子信息
<br>①输入 CCZDC(String frequency)

该输入方法在类Protocal21Write下，调用方法如下：

```java
//终端信息自检输出，3秒一次  
BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCZDC("3"));
```
| 参数 | 说明 |
|--|--|
|frequency  |自检频度  |

<br> ②输出 BDZDX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

| 参数 |说明  |
|----|----|
| var0 | 十六进制原始指令 |
|var1[1]|北斗卡号码|
| var1[2] | 电池电量 |
|var1[3] | 波速1信号 |
|var1[4] |波速2信号 |
|var1[5] |波速3信号 |
|var1[6] |波速4信号 |
|var1[7] |波速5信号 |
|var1[8] |波速6信号 |
|var1[9] |波速7信号|
|var1[10] |波速8信号|
|var1[11] |波速9信号|
|var1[12] |波速10信号|
|var1[13] |服务频度|
|var1[14] |北斗卡等级|
|var1[15] |通讯长度|


#### 2.查询当前系统工作模式



<br>①输入 CCMSC()


该输入方法在类Protocal21Write下，调用方法如下：

```java
//查询系统当前工作模式  
BeidouSDKManage.getInstance().sendHexCommand(Protocal21Write.getInstance().CCMSC());
```


<br>②输出 BDMSH(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

| 参数 |说明  |
|--|--|
|var0  | 十六进制原始指令 |
|var1[1]  | 0.表示无工作模式<br>1.表示正常工作模式（正常开机后，系统默认进入的工作模式）<br>2 表示系统处于SOS工作模式<br>3 表示系统处于极限追踪－正常模式<br>4 表示系统处于极限追踪－省电模式<br>5 表示系统处于极限追踪－定位模式<br>6 表示系统处于按键触发模式<br>7 表示系统处于关闭模式<br>8表示系统处于OK工作模式 |

#### 3.通信申请
##### 3.1 发送短报文
如何发射北斗短报文，发送北斗短板文时请将北斗设备的天线朝南开阔的地方，并倾斜25°~45°，具体角度开发者根据自己当前地区调整，调整至北斗设备的“信号灯”至绿色。  

北斗短报文通信有频度限制，请开发者控制北斗短报文频度发送控制。

北斗通信报文形式有3种，请开发者使用北斗通信之前稍微了解一下：

* 汉字模式
* 代码模式
* 代码和汉字混合模式


```java
//1、发送混合模式北斗短报文
* @param toNumber 接收卡号  
* @param content GB18030内的编码  
BeidouSDKManage.getInstance().sendMessage("190077","你好吗？");

//sendMessage()方法是使用混合模式发送，实际调用下面方法（通信申请）
//BeidouSDKManage.getInstance().sendTXA("190077",1,2,"A4C4E3BAC3C2F0A3BF");

//2、发送通信申请
* @param toNumber
* @param type 信息类型	1:普通通信，2:预留，3:通信查询 （北斗4.0协议固定填1）
* @param form 报文形式	0：汉字，1:代码，2:汉字和代码混合
* @param content 报文内容：如果form为0（汉字形式），则填GB18030内的编码，否则填十六进制字符串，如果form为2（混合形式），请在内容前面加上A4，否则卫星系统自动置换前面一个字节为A4
BeidouSDKManage.getInstance().sendTXA(toNumber,type,form,content);

//通信申请注释请认真读，特别是content的说明

两个方法发送都是发短报文，
如果开发者不需要自定义报文内容协议，那么使用sendMessage()混合模式方法即可以，收到报文后会回调receivedMessage()方法，同时先回调BDTXR()方法，因为sendMessage()实际使用混合模式形式sendTXA()方法。

如果开发者需要自定义报文内容协议，那么请使用sendTXA()通信申请方法，收到报文后会回调BDTXR()方法
```

##### 3.2 发送反馈

北斗通信的过程是这样的：

![MacDown Screenshot](http://pic.pancoit.com/PanGuYum/tdwt/image/1576909383414-1383373467.png)

其中在第1过程和第4过程中发生丢包的可能性是比较高的，所以发射北斗短报文需要有良好的信号，接收短报文也需要有良好的北斗信号。

信息反馈只能反馈第1过程中北斗设备是否已经将北斗通信内容发射出去，并不能判断是否送达卫星，更不能判断第2、3、4步骤是否到达。

请设置终端数据监听 BeidouSDKManage.getInstance().setProtocolListener()

```java
/**
     * 反馈信息
     *
     * @param var0 十六进制指令
     *             var1[1] 相关语句标识符 "TXA" , “DWA” 等
     *             var1[2] 指令执行情况 Y:指令发射成功  N:指令发射失败
     *             var1[3] 语句未正确响应的代码标识
     *             var1[4] 语句未正确响应原因的文字描述
     */
    void BDFKI(String var0, String[] var1);
    
// 【非4.0协议请忽略】如果是使用北斗4.0协议监听器，那么实现void FKXX(String var0, int flag, String info);具体请查看sdk备注
```

##### 3.3 接收短报文

请设置终端数据监听 BeidouSDKManage.getInstance().setProtocolListener()

```java
1、接收混合模式短报文，SDK将接收到的内容自动解析为GB18030编码内容
/**
  * 接收到消息（只有混合模式的时候才会调用）
  *
  * @param formNumber 发信方的北斗卡号
  * @param content 内容（GB18030编码）
  */
  void receivedMessage(String formNumber, String content);
 
 2、接收通信申请，SDK将接收到通信申请原始数据回调，回调receivedMessage()方法之前一定会先回调这个方法，因为BDTXR是接收到的原始报文内容。
 / ** 报文通信信息
    *
    * @param var0 十六进制指令
    *             var1[1] 信息类型
    *             var1[2] 发信方用户地址ID号
    *             var1[3] 报文形式
    *             var1[4] 发信时间（UTC）,有问题，不推荐使用
    *             var1[5] 报文通信信息内容
    */
    void BDTXR(String var0, String[] var1);
    
// 【非4.0协议请忽略】如果是使用北斗4.0协议监听器，那么实现void TXXX(String var0, String[] var1);具体请查看sdk备注
```

####  4.SOS模块

 - 查询/设置
<br>①输入CCSHM(String operation, String serverCardNum, String freq, String content)
该输入方法在类Protocal21Write下。

| 参数 | 说明 |
|----|----|
|operation  | ‘0’:查询，为’0’时，无后续字段（设置为null或者‘’）。<br>‘1’:设置当前SOS的中心号码、频度和内容。  |
|serverCardNum  | 中心号码|
|freq  | 频度|
|content  | 内容|

②输出BDHMX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

| 参数 |说明  |
|----|----|
| var0 |十六进制原始指令  |
| var1[1] |中心号码  |
| var1[2]|频度  |
| var1[3]|内容  |

 - 启动/关闭SOS
 <br>①输入 CCQJY(String operation)
 该输入方法在类Protocal21Write下。
 
| 参数 | 说明 |
|----|----|
| operation | ‘0’:关闭<br>‘1’:启动 |

<br>②输出 BDQDX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

| 参数 | 说明 |
|----|----|
| var0 | 十六进制原始指令 |
| var1[1] | 错误码:<br>‘0’:成功进入/退出SOS<br>‘1’:无上报中心号码<br>‘2’:系统已工作在SOS模式<br>‘3’:未读取到北斗卡，SOS无法工作 |

#### 5.极限追踪模块

 - 查询/设置
 
①输入 CCZZM(String operation, String serverCardNum, String freq, String limitMode)
该输入方法在类Protocal21Write下。

|参数  | 说明 |
|----|----|
|operation  |‘0’:查询，为’0’时，无后续字段。<br>‘1’:设置当前SOS的中心号码、频度和模式。  |
|serverCardNum|中心号码|
|freq|频度|
|limitMode|模式：默认为正常模式<br>1:正常模式，RD开RN开<br>2:省电模式，RD开RN关<br>3:定位模式，RD关RN开|

②输出 BDZZX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

| 参数 |说明  |
|----|----|
| var0 | 十六进制原始指令 |
| var1[1] | 中心号码 |
| var1[2] | 频度 |
| var1[3] | 模式：<br>1:正常模式，RD开RN开<br>2:省电模式，RD开RN关<br>3:定位模式，RD关RN开 |

 - 启动/关闭
 
①输入CCQZZ(String operation)
该输入方法在类Protocal21Write下。

|参数  | 说明 |
|----|----|
|operation  |‘0’：关闭<br>‘1’：启动  |

②输出 BDQZX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

|参数| 说明 |
|----|----|
| var0 |十六进制原始指令  |
| var1[1] |错误码：<br>‘0’：成功进入/退出极限追踪<br>‘1’：无上报中心号码<br>‘2’：系统已工作在SOS模式<br>‘3’：未读取到北斗卡，极限追踪无法工作  |

#### 6.OK键模块

 - 查询/设置
 
①输入 CCOKS(String operation, String serverCardNum, String content)
该输入方法在类Protocal21Write下。

| 参数 | 说明 |
|----|----|
|operation  |‘0’：查询，为’0’时，无后续字段。<br> ‘1’：设置当前OK键的中心号码、内容。 |
|serverCardNum  |中心号码 |
|content  |内容 |

②输出 BDOKX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

|参数  |说明  |
|----|----|
|var0  |十六进制原始指令  |
|var1[1] |中心号码  |
|var1[2] |内容  |

 - 启动/关闭
<br> ①输入 CCQOK(String type)
该输入方法在类Protocal21Write下。

|参数  |说明  |
|----|----|
|operation  |‘0’：关闭<br>‘1’：启动  |

#### 7.FN功能键设置

①输入 CCFNS(String operation)
该输入方法在类Protocal21Write下。

|参数  |说明  |
|----|----|
|operation  |‘1’：OK报平安键<br>‘2’：启动极限追踪键  |


②输出 BDFNX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

|参数  |说明  |
|----|----|
|var0 |十六进制原始指令  |
|var1[1] |‘1’：OK报平安键<br>‘2’：启动极限追踪键  |

#### 8.版本查询
①输入 CCVRQ()
该输入方法在类Protocal21Write下。

②输出 BDVRX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

|参数  |说明  |
|----|----|
|var0  |十六进制原始指令  |
|var1[1] |版本信息  |

#### 9.蓝牙名称设置
①输入 CCBTI(String bluetoothName)
该输入方法在类Protocal21Write下。

|参数  |说明  |
|----|----|
|bluetoothName  |蓝牙名称，最大11字节  |
②输出

设置成功后，会自动断开蓝牙连接。

#### 10.未读消息
①输入 CCMSQ(String operation)

|参数  | 说明 |
|----|----|
|operation  |‘0’：读未读短信息条数<br>‘1’：单条读取<br>‘2’：批量读取  |

②未读条数输出 BDMSX(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

|参数  |说明  |
|----|----|
|var0  |十六进制原始指令  |
|var1[1]  |剩余未读短信息的条数  |

③内容输出 BDTXR(String var0, String[] var1)
该输出在监听器I21ProtocolListener下。

|参数  |说明  |
|----|----|
|var0  |十六进制原始指令  |
|var1[1]  |信息类型  |
|var1[2] |发送方号码  |
|var1[3] |报文形式：<br>‘0’：汉字<br>'1'：代码<br>‘2’：汉字混合代码  |
|var1[4] |发信时间（UTC）  |
|var1[5] |内容  |
